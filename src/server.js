const express = require('express');
const http = require('http');
const socketIo = require('socket.io');
const setupMiddleware = require('./middleware');
const setupRoutes = require('./routes');
const logger = require('./utils/logger');

const app = express();
setupMiddleware(app);
setupRoutes(app);

const PORT = process.env.PGS_PORT;
if (!PORT) {
  logger.error('FATAL ERROR: PORT is not defined');
  process.exit(1);
}

const server = http.createServer(app);
const io = socketIo(server);
app.set('socketIo', io);
server.listen(PORT, () => logger.info(`PGS system started successfully`));
