const logger = require('../utils/logger');

module.exports = () => {
  process.on('uncaughtException', (ex) => {
    logger.error(`UncaughtException: ${ex.message}`, ex);
    process.exit(1);
  });

  process.on('unhandledRejection', (ex) => {
    logger.error(`UnhandledRejection: ${ex.message}`, ex);
    process.exit(1);
  });
};

