const express = require("express");
const morgan = require("morgan");
const cors = require("cors");
const helmet = require('helmet');
const setupUnhandledExceptionsHandler = require('./unhandledExceptionsHandler');

module.exports = (app) => {
  setupUnhandledExceptionsHandler();
  app.use(cors());
  app.use(helmet());
  app.use(express.json());
  app.use(express.urlencoded({extended: false}));
  app.use(morgan('dev'));
};