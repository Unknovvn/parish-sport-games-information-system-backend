const {byRoleIdQuery, createUserCommand, updateUserPasswordCommand} = require('../models/user');
const {byIdQuery} = require('../models/common');

module.exports = ({userService}) => {
  const getAll = async (req, res) => {
    const users = await userService.getAllUsers();
    return res.status(200).json(users);
  };

  const getById = async (req, res) => {
    const query = byIdQuery(req.params);
    if (query.validationErrors)
      return res.status(400).json({error: query.validationErrors});

    const user = await userService.getUserById(query);
    if (user)
      return res.status(200).json(user);

    return res.status(400).json({error: [`Nėra vartotojo su id = ${query.id}`]});
  };

  const getByRole = async (req, res) => {
    const query = byRoleIdQuery(req.params);
    if (query.validationErrors)
      return res.status(400).json({error: query.validationErrors});

    const users = await userService.getUsersByRole(query);
    return res.status(200).json(users);
  };

  const create = async (req, res) => {
    const command = createUserCommand(req.body);
    if (command.validationErrors)
      return res.status(400).json({error: command.validationErrors});

    const user = await userService.createUser(command);
    if (user.error)
      return res.status(400).json({error: [user.error]});

    return res.status(201).json(user);
  };

  const editPassword = async (req, res) => {
    const query = byIdQuery(req.params);
    if (query.validationErrors)
      return res.status(400).json({error: query.validationErrors});

    const command = updateUserPasswordCommand(req.body);
    if (command.validationErrors)
      return res.status(400).json({error: command.validationErrors});

    if (await userService.updatePassword(query, command))
      return res.status(200).json('Vartotojo duomenys sėkmingai redaguotas');

    return res.status(400).json({error: [`Nepavyko redaguoti vartotojo su id = ${query.id} duomenys`]})
  };

  const remove = async (req, res) => {
    const command = byIdQuery(req.params);
    if (command.validationErrors)
      return res.status(400).json({error: command.validationErrors});

    const errorMessage = await userService.deleteUser(command);
    if (errorMessage === '')
      return res.status(200).json('Vartotojas sėkmingai ištrintas');

    return res.status(400).json({error: [errorMessage]});
  };

  return Object.freeze({
    getAll,
    getById,
    getByRole,
    create,
    editPassword,
    remove
  })
};