const {byChampionshipIdAndCoachIdQuery, createTeamCommand, editTeamCommand} = require('../models/team');
const {byIdQuery} = require('../models/common');

module.exports = ({teamService}) => {
  const getCoachesTeam = async (req, res) => {
    const query = byChampionshipIdAndCoachIdQuery(req.params);
    if (query.validationErrors)
      return res.status(400).json({error: query.validationErrors});

    const team = await teamService.getByChampionshipIdAndCoachId(query);
    return res.status(200).json(team);
  };

  const create = async (req, res) => {
    const command = createTeamCommand(req.body);
    if (command.validationErrors)
      return res.status(400).json({error: command.validationErrors});

    const team = await teamService.create(command);
    if (team.error)
      return res.status(400).json({error: [team.error]});

    return res.status(201).json(team);
  };

  const edit = async (req, res) => {
    const query = byIdQuery(req.params);
    if (query.validationErrors)
      return res.status(400).json({error: query.validationErrors});

    const command = editTeamCommand(req.body);
    if (command.validationErrors)
      return res.status(400).json({error: command.validationErrors});

    if (await teamService.update(query, command))
      return res.status(200).json('Varžybos sėkmingai redaguotos');

    return res.status(400).json({error: [`Nepavyko redaguoti varžybų su id = ${query.id}`]})
  };

  const remove = async (req, res) => {
    const command = byIdQuery(req.params);
    if (command.validationErrors)
      return res.status(400).json({error: command.validationErrors});

    const errorMessage = await teamService.delete(command);
    if (errorMessage === '')
      return res.status(200).json('Komanda sėkmingai ištrinta');

    return res.status(400).json({error: [errorMessage]});
  };

  return Object.freeze({
    getCoachesTeam,
    create,
    edit,
    remove
  });
}