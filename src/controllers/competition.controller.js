const {byIdQuery} = require('../models/common');
const {
  byChampionshipIdQuery,
  byCompetitionIdQuery,
  byChampionshipIdAndCoachIdQuery,
  byChampionshipIdAndRefereeIdQuery,
  createCompetitionCommand,
  editCompetitionCommand,
  registerTeamCommand,
  registerTeamMemberCommand,
  finishCompetitionCommand
} = require('../models/competition');

module.exports = ({competitionService}) => {
  const getById = async (req, res) => {
    const query = byIdQuery(req.params);
    if (query.validationErrors)
      return res.status(400).json({error: query.validationErrors});

    const competition = await competitionService.getById(query);
    if (competition)
      return res.status(200).json(competition);

    return res.status(400).json({error: [`Nėra varžybų su id = ${query.id}`]})
  };

  const getByChampionship = async (req, res) => {
    const query = byChampionshipIdQuery(req.params);
    if (query.validationErrors)
      return res.status(400).json({error: query.validationErrors});

    const competitions = await competitionService.getByChampionshipId(query);
    return res.status(200).json(competitions);
  };

  const getCoachesCompetitions = async (req, res) => {
    const query = byChampionshipIdAndCoachIdQuery(req.params);
    if (query.validationErrors)
      return res.status(400).json({error: query.validationErrors});

    const competitions = await competitionService.getCoachesCompetitions(query);
    return res.status(200).json(competitions);
  };

  const getRefereesCompetitions = async (req, res) => {
    const query = byChampionshipIdAndRefereeIdQuery(req.params);
    if (query.validationErrors)
      return res.status(400).json({error: query.validationErrors});

    const competitions = await competitionService.getRefereesCompetitions(query);
    return res.status(200).json(competitions);
  };

  const getSoloCompetitors = async (req, res) => {
    const query = byCompetitionIdQuery(req.params);
    if (query.validationErrors)
      return res.status(400).json({error: query.validationErrors});

    const competitions = await competitionService.getSoloCompetitors(query);
    return res.status(200).json(competitions);
  };

  const getTeamCompetitors = async (req, res) => {
    const query = byCompetitionIdQuery(req.params);
    if (query.validationErrors)
      return res.status(400).json({error: query.validationErrors});

    const competitions = await competitionService.getTeamCompetitors(query);
    return res.status(200).json(competitions);
  };

  const create = async (req, res) => {
    const command = createCompetitionCommand(req.body);
    if (command.validationErrors)
      return res.status(400).json({error: command.validationErrors});

    const competition = await competitionService.createCompetition(command);
    if (competition.error)
      return res.status(400).json({error: [competition.error]});

    return res.status(201).json(competition);
  };

  const registerTeam = async (req, res) => {
    const command = registerTeamCommand(req.body);
    if (command.validationErrors)
      return res.status(400).json({error: command.validationErrors});

    const registration = await competitionService.registerTeam(command);
    if (registration.error)
      return res.status(400).json({error: [registration.error]});

    return res.status(201).json('Komanda sėkmingai registruota į varžybas');
  };

  const registerTeamMember = async (req, res) => {
    const command = registerTeamMemberCommand(req.body);
    if (command.validationErrors)
      return res.status(400).json({error: command.validationErrors});

    const registration = await competitionService.registerTeamMember(command);
    if (registration.error)
      return res.status(400).json({error: [registration.error]});

    return res.status(201).json('Komanda ir komandos narys sėkmingai registruoti į varžybas');
  };

  const activate = async (req, res) => {
    const query = byIdQuery(req.params);
    if (query.validationErrors)
      return res.status(400).json({error: query.validationErrors});

    if (await competitionService.activateCompetition(query))
      return res.status(200).json('Varžybos sėkmingai aktyvuotos');

    return res.status(400).json({error: [`Nepavyko aktyvuoti varžybų su id = ${query.id}`]})
  };

  const finishSolo = async (req, res) => {
    const query = byIdQuery(req.params);
    if (query.validationErrors)
      return res.status(400).json({error: query.validationErrors});

    const command = finishCompetitionCommand(req.body);
    if (command.validationErrors)
      return res.status(400).json({error: command.validationErrors});

    if (await competitionService.finishSoloCompetition(query, command)) {
      const io = req.app.get('socketIo');
      io.sockets.emit('competitionFinished');
      return res.status(200).json('Varžybos sėkmingai užbaigtos');
    }

    return res.status(400).json({error: [`Nepavyko užbaigti varžybų su id = ${query.id}`]})
  };

  const finishTeam = async (req, res) => {
    const query = byIdQuery(req.params);
    if (query.validationErrors)
      return res.status(400).json({error: query.validationErrors});

    const command = finishCompetitionCommand(req.body);
    if (command.validationErrors)
      return res.status(400).json({error: command.validationErrors});

    if (await competitionService.finishTeamCompetition(query, command)) {
      const io = req.app.get('socketIo');
      io.sockets.emit('competitionFinished');
      return res.status(200).json('Varžybos sėkmingai užbaigtos');
    }

    return res.status(400).json({error: [`Nepavyko užbaigti varžybų su id = ${query.id}`]})
  };

  const edit = async (req, res) => {
    const query = byIdQuery(req.params);
    if (query.validationErrors)
      return res.status(400).json({error: query.validationErrors});

    const command = editCompetitionCommand(req.body);
    if (command.validationErrors)
      return res.status(400).json({error: command.validationErrors});

    if (await competitionService.editCompetition(query, command))
      return res.status(200).json('Varžybos sėkmingai redaguotos');

    return res.status(400).json({error: [`Nepavyko redaguoti varžybų su id = ${query.id}`]})
  };

  const remove = async (req, res) => {
    const query = byIdQuery(req.params);
    if (query.validationErrors)
      return res.status(400).json({error: query.validationErrors});

    const errorMessage = await competitionService.deleteCompetition(query);
    if (errorMessage === '')
      return res.status(200).json('Varžybos sėkmingai ištrintos');

    return res.status(400).json({error: [errorMessage]});
  };

  return Object.freeze({
    getById,
    getByChampionship,
    getCoachesCompetitions,
    getRefereesCompetitions,
    getSoloCompetitors,
    getTeamCompetitors,
    create,
    registerTeam,
    registerTeamMember,
    activate,
    finishSolo,
    finishTeam,
    edit,
    remove
  })
};