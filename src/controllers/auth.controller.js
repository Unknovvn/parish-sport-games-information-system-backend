const {loginCommand} = require('../models/authentication');

module.exports = ({authenticationService}) => {
  const login = async (req, res) => {
    const command = loginCommand(req.body);
    if (command.validationErrors)
      return res.status(400).json({error: command.validationErrors});

    const token = await authenticationService.authenticateUser(command);
    if (token)
      return res
        .header('x-auth-token', token)
        .header('access-control-expose-headers', 'x-auth-token')
        .status(200)
        .json();

    return res.status(401).json();
  };

  return Object.freeze({
    login
  });
};