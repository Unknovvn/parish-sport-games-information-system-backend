const {byIdQuery} = require('../models/common');
const {
  byCoachIdQuery,
  createChampionshipCommand,
  editChampionshipCommand
} = require('../models/championship');

module.exports = ({championshipService}) => {
  const getAll = async (req, res) => {
    const championships = await championshipService.get();
    return res.status(200).json(championships);
  };

  const getCoachesChampionship = async (req, res) => {
    const query = byCoachIdQuery(req.params);
    if (query.validationErrors)
      return res.status(400).json({error: query.validationErrors});

    const championships = await championshipService.getCoachesChampionships(query);
    return res.status(200).json(championships);
  };

  const getActive = async (req, res) => {
    const championships = await championshipService.getActive();
    return res.status(200).json(championships);
  };

  const getFinished = async (req, res) => {
    const championships = await championshipService.getFinished();
    return res.status(200).json(championships);
  };

  const getResults = async (req, res) => {
    const query = byIdQuery(req.params);
    if (query.validationErrors)
      return res.status(400).json({error: query.validationErrors});

    const results = await championshipService.getResults(query);
    return res.status(200).json(results);
  };

  const create = async (req, res) => {
    const command = createChampionshipCommand(req.body);
    if (command.validationErrors)
      return res.status(400).json({error: command.validationErrors});

    const championship = await championshipService.create(command);
    if (championship.error)
      return res.status(400).json({error: [championship.error]});

    return res.status(201).json(championship);
  };

  const activate = async (req, res) => {
    const query = byIdQuery(req.params);
    if (query.validationErrors)
      return res.status(400).json({error: query.validationErrors});

    if (await championshipService.activate(query))
      return res.status(200).json('Čempionatas sėkmingai aktyvuotas');

    return res.status(400).json({error: [`Nepavyko aktyvuoti čempionato su id = ${query.id}`]})
  };

  const finish = async (req, res) => {
    const query = byIdQuery(req.params);
    if (query.validationErrors)
      return res.status(400).json({error: query.validationErrors});

    if (await championshipService.finish(query))
      return res.status(200).json('Čempionatas sėkmingai užbaigtas');

    return res.status(400).json({error: [`Nepavyko užbaigti čempionato su id = ${query.id}`]})
  };

  const edit = async (req, res) => {
    const query = byIdQuery(req.params);
    if (query.validationErrors)
      return res.status(400).json({error: query.validationErrors});

    const command = editChampionshipCommand(req.body);
    if (command.validationErrors)
      return res.status(400).json({error: command.validationErrors});

    if (await championshipService.edit(query, command))
      return res.status(200).json('Čempionatas sėkmingai redaguotas');

    return res.status(400).json({error: [`Nepavyko redaguoti čempionato su id = ${query.id}`]})
  };

  const remove = async (req, res) => {
    const query = byIdQuery(req.params);
    if (query.validationErrors)
      return res.status(400).json({error: query.validationErrors});

    const errorMessage = await championshipService.delete(query);
    if (errorMessage === '')
      return res.status(200).json('Čempionatas sėkmingai ištrintas');

    return res.status(400).json({error: [errorMessage]});
  };

  return Object.freeze({
    getAll,
    getCoachesChampionship,
    getActive,
    getFinished,
    getResults,
    create,
    activate,
    finish,
    edit,
    remove
  })
};