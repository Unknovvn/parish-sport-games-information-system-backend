const userController = require('./user.controller');
const championshipController = require('./championship.controller');
const competitionController = require('./competition.controller');
const teamController = require('./team.controller');
const authController = require('./auth.controller');
const userService = require('../services/user');
const championshipService = require('../services/championship');
const competitionService = require('../services/competition');
const teamService = require('../services/team');
const authenticationService = require('../services/authentication');

module.exports = Object.freeze({
  authController: authController({authenticationService}),
  championshipController: championshipController({championshipService}),
  competitionController: competitionController({competitionService}),
  teamController: teamController({teamService}),
  userController: userController({userService})
});