const errorHandler = require('../middleware/errorHandler');
const userRoutes = require('./user');
const championshipRoutes = require('./championship');
const competitionRoutes = require('./competition');
const teamRoutes = require('./team');
const authRoutes = require('./auth');

module.exports = app => {
  app.use('/api/user', userRoutes);
  app.use('/api/championship', championshipRoutes);
  app.use('/api/competition', competitionRoutes);
  app.use('/api/team', teamRoutes);
  app.use('/api/auth', authRoutes);
  app.use(errorHandler);
}