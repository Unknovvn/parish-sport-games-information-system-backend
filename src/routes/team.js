const {Router} = require('express');
const asyncHandler = require('../middleware/asyncHandler');
const {teamController} = require('../controllers');

const router = Router();
router.get('/:championshipId/:coachId', asyncHandler(teamController.getCoachesTeam));
router.post('/', asyncHandler(teamController.create));
router.put('/:id', asyncHandler(teamController.edit));
router.delete('/:id', asyncHandler(teamController.remove));

module.exports = router;