const {Router} = require('express');
const asyncHandler = require('../middleware/asyncHandler');
const {userController} = require('../controllers');

const router = Router();
router.get('/', asyncHandler(userController.getAll));
router.get('/:id', asyncHandler(userController.getById));
router.get('/byRole/:roleId', asyncHandler(userController.getByRole));
router.post('/', asyncHandler(userController.create));
router.put('/password/:id', asyncHandler(userController.editPassword));
router.delete('/:id', asyncHandler(userController.remove));

module.exports = router;
