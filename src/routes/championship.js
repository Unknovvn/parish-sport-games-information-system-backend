const {Router} = require('express');
const asyncHandler = require('../middleware/asyncHandler');
const {championshipController} = require('../controllers');

const router = Router();
router.get('/', asyncHandler(championshipController.getAll));
router.get('/coach/:coachId', asyncHandler(championshipController.getCoachesChampionship));
router.get('/active', asyncHandler(championshipController.getActive));
router.get('/finished', asyncHandler(championshipController.getFinished));
router.get('/:id/results', asyncHandler(championshipController.getResults));
router.post('/', asyncHandler(championshipController.create));
router.put('/activate/:id', asyncHandler(championshipController.activate));
router.put('/finish/:id', asyncHandler(championshipController.finish));
router.put('/:id', asyncHandler(championshipController.edit));
router.delete('/:id', asyncHandler(championshipController.remove));

module.exports = router;