const {Router} = require('express');
const asyncHandler = require('../middleware/asyncHandler');
const {authController} = require('../controllers');

const router = Router();
router.post('/login', asyncHandler(authController.login));

module.exports = router;