const {Router} = require('express');
const asyncHandler = require('../middleware/asyncHandler');
const {competitionController} = require('../controllers');

const router = Router();
router.get('/:id', asyncHandler(competitionController.getById));
router.get('/byChampionship/:championshipId', asyncHandler(competitionController.getByChampionship));
router.get('/:championshipId/coach/:coachId', asyncHandler(competitionController.getCoachesCompetitions));
router.get('/:championshipId/referee/:refereeId', asyncHandler(competitionController.getRefereesCompetitions));
router.get('/:competitionId/solo-competitors', asyncHandler(competitionController.getSoloCompetitors));
router.get('/:competitionId/team-competitors', asyncHandler(competitionController.getTeamCompetitors));
router.post('/', asyncHandler(competitionController.create));
router.post('/registerTeam', asyncHandler(competitionController.registerTeam));
router.post('/registerTeamMember', asyncHandler(competitionController.registerTeamMember));
router.put('/activate/:id', asyncHandler(competitionController.activate));
router.put('/finish-solo/:id', asyncHandler(competitionController.finishSolo));
router.put('/finish-team/:id', asyncHandler(competitionController.finishTeam));
router.put('/:id', asyncHandler(competitionController.edit));
router.delete('/:id', asyncHandler(competitionController.remove));

module.exports = router;