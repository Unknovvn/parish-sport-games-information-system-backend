const winston = require('winston');

const logger = winston.createLogger({
  format: winston.format.json(),
  transports: [
    new winston.transports.File({filename: 'logs/PSGBackEnd.log', level: 'error'})
  ]
});

if (process.env.NODE_ENV !== 'production') {
  // noinspection JSCheckFunctionSignatures
  logger.add(new winston.transports.Console({
    format: winston.format.simple()
  }));
}

module.exports = logger;