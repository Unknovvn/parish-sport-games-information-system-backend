module.exports = (validationSchema, data) => {
  const {error} = validationSchema.validate(data, {abortEarly: false});
  if (error && error.details.length > 0)
    return error.details.map(detail => detail.message);

  return null;
};