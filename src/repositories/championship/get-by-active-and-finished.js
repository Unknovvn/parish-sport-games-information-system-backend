module.exports = ({database}) => {
  return async (includeActive, includeFinished) => {
    const command =
        `SELECT id, name, date, active, finished
       FROM championships
       WHERE active = $1 AND finished = $2`;

    const parameters = [includeActive, includeFinished];
    const dbResponse = await database.query(command, parameters);
    return dbResponse.rows;
  };
};