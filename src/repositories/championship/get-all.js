module.exports = ({database}) => {
  return async () => {
    const command =
      `SELECT id, name, date, active, finished
       FROM championships`;

    const dbResponse = await database.query(command, []);
    return dbResponse.rows;
  };
};