module.exports = ({database}) => {
  return async (championshipId) => {
    const command =
      `DELETE 
       FROM championships 
       WHERE id = $1`;

    const parameters = [championshipId];
    const dbResponse = await database.query(command, parameters);
    return dbResponse.rowCount !== 0;
  };
};