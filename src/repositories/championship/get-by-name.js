module.exports = ({database}) => {
  return async (championshipName) => {
    const command =
      `SELECT id
       FROM championships
       WHERE name = $1`;

    const parameters = [championshipName];
    const dbResponse = await database.query(command, parameters);
    return dbResponse.rows[0];
  };
};