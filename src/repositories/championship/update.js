module.exports = ({database}) => {
  return async (name, date, championshipId) => {
    const command =
      `UPDATE championships 
       SET 
        name = $1,
        date = $2
       WHERE id = $3`;

    const parameters = [name, date, championshipId];
    const dbResponse = await database.query(command, parameters);
    return dbResponse.rowCount !== 0;
  };
};