const database = require('../database');
const buildGetChampionshipByName = require('./get-by-name');
const buildGetAllChampionships = require('./get-all');
const buildGetCoachesChampionships = require('./get-coaches-championships');
const buildGetByActiveAndFinished = require('./get-by-active-and-finished');
const buildInsertChampionship = require('./insert');
const buildDeleteChampionship = require('./delete');
const buildUpdateChampionship = require('./update');
const buildActivateChampionship = require('./activate');
const buildFinishChampionship = require('./finish');

module.exports = Object.freeze({
  getChampionshipByName: buildGetChampionshipByName({database}),
  getAllChampionships: buildGetAllChampionships({database}),
  getCoachesChampionships: buildGetCoachesChampionships({database}),
  getByActiveAndFinished: buildGetByActiveAndFinished({database}),
  insertChampionship: buildInsertChampionship({database}),
  deleteChampionship: buildDeleteChampionship({database}),
  updateChampionship: buildUpdateChampionship({database}),
  activateChampionship: buildActivateChampionship({database}),
  finishChampionship: buildFinishChampionship({database})
});