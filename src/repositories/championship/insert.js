module.exports = ({database}) =>
  async (name, date, active, finished) => {
    const command =
      `INSERT INTO 
       championships (name, date, active, finished)
       VALUES ($1, $2, $3, $4)
       RETURNING id`;

    const parameters = [name, date, active, finished];
    const dbResponse = await database.query(command, parameters);
    return dbResponse.rows[0];
  };