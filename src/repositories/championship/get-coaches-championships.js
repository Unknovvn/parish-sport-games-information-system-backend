module.exports = ({database}) => {
  return async (coachId) => {
    const command =
        `SELECT id, name, date, active, finished
       FROM championships
       WHERE id IN 
        (
            SELECT championship_id
            FROM teams
            WHERE coach_id = $1
        )`;

    const parameters = [coachId];
    const dbResponse = await database.query(command, parameters);
    return dbResponse.rows;
  };
};