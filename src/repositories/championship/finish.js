module.exports = ({database}) => {
  return async (championshipId) => {
    const command =
      `UPDATE championships 
       SET 
        active = false,
        finished = true
       WHERE id = $1`;

    const parameters = [championshipId];
    const dbResponse = await database.query(command, parameters);
    return dbResponse.rowCount !== 0;
  };
};