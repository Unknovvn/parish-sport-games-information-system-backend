module.exports = ({database}) =>
  async (name, surname, teamId) => {
    const command =
        `SELECT id
       FROM team_members
       WHERE  name = $1
        AND surname = $2
        AND team_id = $3`;

    const parameters = [name, surname, teamId];
    const dbResponse = await database.query(command, parameters);
    return dbResponse.rows[0];
  };