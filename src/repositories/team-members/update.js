module.exports = ({database}) => {
  return async (teamMemberId, name, surname) => {
    const command =
        `UPDATE team_members 
       SET 
        name = $2,
        surname = $3
       WHERE id = $1`;

    const parameters = [teamMemberId, name, surname];
    const dbResponse = await database.query(command, parameters);
    return dbResponse.rowCount !== 0;
  };
};