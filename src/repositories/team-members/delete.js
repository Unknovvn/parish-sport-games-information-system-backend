module.exports = ({database}) => {
  return async (teamMemberId) => {
    const command =
        `DELETE 
       FROM team_members
       WHERE id = $1`;

    const parameters = [teamMemberId];
    const dbResponse = await database.query(command, parameters);
    return dbResponse.rowCount !== 0;
  };
};