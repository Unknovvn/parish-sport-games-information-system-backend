module.exports = ({database}) =>
  async (name, surname, teamId) => {
    const command =
        `INSERT INTO 
       team_members (name, surname, team_id)
       VALUES ($1, $2, $3)`;

    const parameters = [name, surname, teamId];
    const dbResponse = await database.query(command, parameters);
    return dbResponse.rows[0];
  };