const database = require('../database');
const buildGetByTeamId = require('./get-by-teamId');
const buildGetByNameSurnameTeamId = require('./get-by-name-surname-teamId');
const buildInsert = require('./insert');
const buildUpdate = require('./update');
const buildDelete = require('./delete');

module.exports = Object.freeze({
  getByTeamId: buildGetByTeamId({database}),
  buildGetByNameSurnameTeamId: buildGetByNameSurnameTeamId({database}),
  insert: buildInsert({database}),
  update: buildUpdate({database}),
  delete: buildDelete({database})
});