module.exports = ({database}) =>
  async (teamId) => {
    const command =
        `SELECT id, name, surname
       FROM team_members
       WHERE team_id = $1`;

    const parameters = [teamId];
    const dbResponse = await database.query(command, parameters);
    return dbResponse.rows;
  };