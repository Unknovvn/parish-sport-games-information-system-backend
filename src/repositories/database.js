const {Pool} = require('pg');
module.exports = new Pool({
  host: process.env.PGS_DB_HOST,
  port: process.env.PGS_DB_PORT,
  database: process.env.PGS_DB_DATABASE,
  user: process.env.PGS_DB_USER,
  password: process.env.PGS_DB_PASSWORD
});