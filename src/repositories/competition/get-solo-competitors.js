module.exports = ({database}) => {
  return async (competitionId) => {
    const command =
        `SELECT id, name, surname
        FROM team_members
        WHERE id IN 
            (
                SELECT team_member_id
                FROM results
                WHERE competition_id = $1
            )`;

    const parameters = [competitionId];
    const dbResponse = await database.query(command, parameters);
    return dbResponse.rows;
  };
};