module.exports = ({database}) => {
  return async (championshipId, name) => {
    const command =
        `SELECT id
       FROM competitions
       WHERE championship_id = $1 AND name = $2`;

    const parameters = [championshipId, name];
    const dbResponse = await database.query(command, parameters);
    return dbResponse.rows[0];
  };
};