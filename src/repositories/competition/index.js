const database = require('../database');
const buildGetById = require('./get-by-id');
const buildGetByChampionshipId = require('./get-by-championshipId');
const buildGetByChampionshipIdAndName = require('./get-by-championshipId-and-name');
const buildGetByChampionshipIdAndRefereeId = require('./get-by-championshipId-and-refereeId');
const buildGetSoloCompetitors = require('./get-solo-competitors');
const buildGetTeamCompetitors = require('./get-team-competitors');
const buildInsertCompetition = require('./insert');
const buildUpdateCompetition = require('./update');
const buildActivateCompetition = require('./activate');
const buildFinishCompetition = require('./finish');
const buildDeleteCompetition = require('./delete');

module.exports = Object.freeze({
  getById: buildGetById({database}),
  getByChampionshipId: buildGetByChampionshipId({database}),
  getByChampionshipIdAndName: buildGetByChampionshipIdAndName({database}),
  getByChampionshipIdAndRefereeId: buildGetByChampionshipIdAndRefereeId({database}),
  getSoloCompetitors: buildGetSoloCompetitors({database}),
  getTeamCompetitors: buildGetTeamCompetitors({database}),
  insertCompetition: buildInsertCompetition({database}),
  updateCompetition: buildUpdateCompetition({database}),
  activateCompetition: buildActivateCompetition({database}),
  finishCompetition: buildFinishCompetition({database}),
  deleteCompetition: buildDeleteCompetition({database})
});