module.exports = ({database}) => {
  return async (championshipId, refereeId) => {
    const command =
        `SELECT id, name, referee_id, result_type_id, active, finished
       FROM competitions
       WHERE championship_id = $1 AND referee_id = $2`;

    const parameters = [championshipId, refereeId];
    const dbResponse = await database.query(command, parameters);
    return dbResponse.rows;
  };
};