module.exports = ({database}) =>
  async (name, refereeId, championshipId, resultTypeId, active, finished) => {
    const command =
      `INSERT INTO 
       competitions (name, referee_id, championship_id, result_type_id, active, finished)
       VALUES ($1, $2, $3, $4, $5, $6)
       RETURNING id`;

    const parameters = [name, refereeId, championshipId, resultTypeId, active, finished];
    const dbResponse = await database.query(command, parameters);
    return dbResponse.rows[0];
  };