module.exports = ({database}) => {
  return async (competitionId) => {
    const command =
        `SELECT id, name
        FROM teams
        WHERE id IN 
            (
                SELECT team_id
                FROM results
                WHERE competition_id = $1
            )`;

    const parameters = [competitionId];
    const dbResponse = await database.query(command, parameters);
    return dbResponse.rows;
  };
};