module.exports = ({database}) => {
  return async (name, refereeId, resultTypeId, competitionId) => {
    const command =
      `UPDATE competitions 
       SET 
        name = $1,
        referee_id = $2,
        result_type_id = $3
       WHERE id = $4`;

    const parameters = [name, refereeId, resultTypeId, competitionId];
    const dbResponse = await database.query(command, parameters);
    return dbResponse.rowCount !== 0;
  };
};