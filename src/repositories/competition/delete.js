module.exports = ({database}) => {
  return async (competitionId) => {
    const command =
      `DELETE 
       FROM competitions 
       WHERE id = $1`;

    const parameters = [competitionId];
    const dbResponse = await database.query(command, parameters);
    return dbResponse.rowCount !== 0;
  };
};