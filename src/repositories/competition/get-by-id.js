module.exports = ({database}) => {
  return async (competitionName) => {
    const command =
      `SELECT id, name, result_type_id
       FROM competitions
       WHERE id = $1`;

    const parameters = [competitionName];
    const dbResponse = await database.query(command, parameters);
    return dbResponse.rows[0];
  };
};