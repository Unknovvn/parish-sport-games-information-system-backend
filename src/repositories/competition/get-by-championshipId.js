module.exports = ({database}) => {
  return async (championshipId) => {
    const command =
        `SELECT id, name, referee_id, result_type_id, active, finished
       FROM competitions
       WHERE championship_id = $1`;

    const parameters = [championshipId];
    const dbResponse = await database.query(command, parameters);
    return dbResponse.rows;
  };
};