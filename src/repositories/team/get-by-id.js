module.exports = ({database}) =>
  async teamId => {
    const command =
        `SELECT id, name
       FROM teams
       WHERE id = $1`;

    const parameters = [teamId];
    const dbResponse = await database.query(command, parameters);
    return dbResponse.rows[0];
  };