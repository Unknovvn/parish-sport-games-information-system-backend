module.exports = ({database}) => {
  return async (teamId) => {
    const command =
        `DELETE 
       FROM teams 
       WHERE id = $1`;

    const parameters = [teamId];
    const dbResponse = await database.query(command, parameters);
    return dbResponse.rowCount !== 0;
  };
};