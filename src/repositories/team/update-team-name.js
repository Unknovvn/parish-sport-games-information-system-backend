module.exports = ({database}) => {
  return async (teamId, name) => {
    const command =
        `UPDATE teams 
       SET name = $1
       WHERE id = $2`;

    const parameters = [name, teamId];
    const dbResponse = await database.query(command, parameters);
    return dbResponse.rowCount !== 0;
  };
};