module.exports = ({database}) =>
  async (championshipId, coachId) => {
    const command =
        `SELECT id, name
       FROM teams
       WHERE championship_id=$1 AND coach_id=$2`;

    const parameters = [championshipId, coachId];
    const dbResponse = await database.query(command, parameters);
    return dbResponse.rows[0];
  };