module.exports = ({database}) =>
  async (name, coachId, championshipId) => {
    const command =
        `INSERT INTO 
       teams (name, coach_id, championship_id)
       VALUES ($1, $2, $3)
       RETURNING id`;

    const parameters = [name, coachId, championshipId];
    const dbResponse = await database.query(command, parameters);
    return dbResponse.rows[0];
  };