const database = require('../database');
const buildGetById = require('./get-by-id');
const buildGetByChampionshipIdAndCoachId = require('./get-team-by-championship-id-and-coach-id');
const buildInsert = require('./insert');
const buildUpdateName = require('./update-team-name');
const buildDelete = require('./delete');

module.exports = Object.freeze({
  getById: buildGetById({database}),
  getByChampionshipIdAndCoachId: buildGetByChampionshipIdAndCoachId({database}),
  insert: buildInsert({database}),
  updateName: buildUpdateName({database}),
  delete: buildDelete({database})
});