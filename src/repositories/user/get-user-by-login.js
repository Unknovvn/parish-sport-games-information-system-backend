module.exports = ({database}) =>
  async (userLogin) => {
    const command =
      `SELECT id, name, surname, role_id, password
       FROM users 
       WHERE login = $1`;

    const parameters = [userLogin];
    const dbResponse = await database.query(command, parameters);
    return dbResponse.rows[0];
  };