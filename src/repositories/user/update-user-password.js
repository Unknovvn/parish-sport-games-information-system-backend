module.exports = ({database}) => {
  return async (userId, password) => {
    const command =
      `UPDATE users 
       SET password = $1
       WHERE id = $2`;

    const parameters = [password, userId];
    const dbResponse = await database.query(command, parameters);
    return dbResponse.rowCount !== 0;
  };
};