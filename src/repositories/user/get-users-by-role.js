module.exports = ({database}) =>
  async (roleId) => {
    const command =
      `SELECT id, name, surname
       FROM users 
       WHERE role_id = $1`;

    const parameters = [roleId];
    const dbResponse = await database.query(command, parameters);
    return dbResponse.rows;
  };