module.exports = ({database}) => {
  return async () => {
    const command =
      `SELECT id, name, surname, role_id
       FROM users`;

    const dbResponse = await database.query(command, []);
    return dbResponse.rows;
  };
};