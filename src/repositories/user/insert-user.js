module.exports = ({database}) =>
  async (name, surname, login, password, roleId) => {
    const command =
      `INSERT INTO 
       USERS (name, surname, login, password, role_id)
       VALUES ($1, $2, $3, $4, $5)
       RETURNING id`;

    const parameters = [name, surname, login, password, roleId];
    const dbResponse = await database.query(command, parameters);
    return dbResponse.rows[0];
  };