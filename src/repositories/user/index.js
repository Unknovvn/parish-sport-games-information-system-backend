const database = require('../database');
const buildGetUserById = require('./get-user-by-id');
const buildGetUserByLogin = require('./get-user-by-login');
const buildGetUsersByRole = require('./get-users-by-role');
const buildGetAllUsers = require('./get-all-users');
const buildDeleteUser = require('./delete-user');
const buildInsertUser = require('./insert-user');
const buildUpdateUserPassword = require('./update-user-password');

module.exports = Object.freeze({
  getUserById: buildGetUserById({database}),
  getUserByLogin: buildGetUserByLogin({database}),
  getUsersByRole: buildGetUsersByRole({database}),
  getAllUsers: buildGetAllUsers({database}),
  deleteUser: buildDeleteUser({database}),
  insertUser: buildInsertUser({database}),
  updateUserPassword: buildUpdateUserPassword({database})
});