module.exports = ({database}) =>
  async (userId) => {
    const command =
      `SELECT id, name, surname, role_id, password
       FROM users
       WHERE id=$1`;

    const parameters = [userId];
    const dbResponse = await database.query(command, parameters);
    return dbResponse.rows[0];
  };