module.exports = ({database}) => {
  return async (userId) => {
    const command =
      `DELETE 
       FROM users 
       WHERE id = $1`;

    const parameters = [userId];
    const dbResponse = await database.query(command, parameters);
    return dbResponse.rowCount !== 0;
  };
};