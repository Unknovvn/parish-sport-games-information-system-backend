module.exports = ({database}) => {
  return async (competitionId, competitorId, result) => {
    const command =
        `UPDATE results 
       SET 
        result = $3
       WHERE competition_id = $1 AND team_member_id = $2`;

    const parameters = [competitionId, competitorId, result];
    const dbResponse = await database.query(command, parameters);
    return dbResponse.rowCount !== 0;
  };
};