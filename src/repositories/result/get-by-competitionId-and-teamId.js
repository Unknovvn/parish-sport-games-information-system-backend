module.exports = ({database}) =>
  async (competitionId, teamId) => {
    const command =
        `SELECT id
       FROM results
       WHERE competition_id=$1 AND team_id=$2`;

    const parameters = [competitionId, teamId];
    const dbResponse = await database.query(command, parameters);
    return dbResponse.rows;
  };