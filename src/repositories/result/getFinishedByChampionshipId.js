module.exports = ({database}) =>
  async championshipId => {
    const command =
        `SELECT id, competition_id, team_id, place, result
       FROM results
       WHERE competition_id IN 
       (
            SELECT id
            FROM competitions
            WHERE finished = true AND championship_id = $1
       )`;

    const parameters = [championshipId];
    const dbResponse = await database.query(command, parameters);
    return dbResponse.rows;
  };