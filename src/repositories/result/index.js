const database = require('../database');
const buildGetByCompetitionIdAndTeamId = require('./get-by-competitionId-and-teamId')
const buildGetFinishedByChampionshipId = require('./getFinishedByChampionshipId');
const buildUpdateSolo = require('./update-solo');
const buildUpdateTeam = require('./update-team');
const buildInsert = require('./insert');

module.exports = Object.freeze({
  getByCompetitionIdAndTeamId: buildGetByCompetitionIdAndTeamId({database}),
  getFinishedByChampionshipId: buildGetFinishedByChampionshipId({database}),
  updateSolo: buildUpdateSolo({database}),
  updateTeam: buildUpdateTeam({database}),
  insert: buildInsert({database})
});