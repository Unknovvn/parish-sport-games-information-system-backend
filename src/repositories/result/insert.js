module.exports = ({database}) =>
  async (competitionId, teamId, teamMemberId) => {
    const command =
        `INSERT INTO 
       results (competition_id, team_id, team_member_id)
       VALUES ($1, $2, $3)
       RETURNING id`;

    const parameters = [competitionId, teamId, teamMemberId];
    const dbResponse = await database.query(command, parameters);
    return dbResponse.rows[0];
  };