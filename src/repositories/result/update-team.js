module.exports = ({database}) => {
  return async (competitionId, teamId, place) => {
    const command =
        `UPDATE results 
       SET 
        place = $3
       WHERE competition_id = $1 AND team_id = $2`;

    const parameters = [competitionId, teamId, place];
    const dbResponse = await database.query(command, parameters);
    return dbResponse.rowCount !== 0;
  };
};