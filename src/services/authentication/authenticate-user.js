module.exports = ({ userRepository, comparePasswordAsync, generateToken }) =>
  async (command) => {
    const user = await userRepository.getUserByLogin(command.username);
    if (!user)
      return false;

    const validCredentials = await comparePasswordAsync(command.password, user.password);
    if (validCredentials)
      return generateToken({ id: user.id, name: user.name, surname: user.surname, role_id: user.role_id });

    return false;
  };
