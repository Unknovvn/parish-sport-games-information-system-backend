const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const userRepository = require('../../repositories/user');
const makeAuthenticateUser = require('./authenticate-user');

const generateToken = (objectInToken) => {
  const webTokenKey = process.env.PGS_WEB_TOKEN_KEY;
  if (!webTokenKey) {
    console.error('FATAL ERROR: PGS_WEB_TOKEN_KEY is not defined');
    process.exit(1);
  }

  return jwt.sign(objectInToken, webTokenKey);
};

const comparePasswordAsync = async (password, hashedPassword) => {
  return await bcrypt.compare(password, hashedPassword);
};

module.exports = Object.freeze({
  authenticateUser: makeAuthenticateUser({userRepository, comparePasswordAsync, generateToken})
});