module.exports = ({userRepository, comparePasswordAsync}) => async (query, command) => {
  const user = await userRepository.getUserById(query.id);
  if (!user)
    return false;

  const validCredentials = await comparePasswordAsync(command.password, user.password);
  if (validCredentials)
    return await userRepository.updateUserPassword(query.id, command.newPassword);

  return false;
};