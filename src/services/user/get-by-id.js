module.exports = ({userRepository}) => async query => {
  const user = await userRepository.getUserById(query.id);
  return {
    id: user.id,
    name: user.name,
    role_id: user.role_id
  };
};