module.exports = ({userRepository}) => async () => {
  const users = await userRepository.getAllUsers();
  return users.map(user => ({
    id: user.id,
    name: user.name,
    surname: user.surname,
    roleId: user.role_id
  }));
};