const userRepository = require('../../repositories/user');
const bcrypt = require('bcrypt');
const buildGetUserById = require('./get-by-id');
const buildGetUsersByRole = require('./get-by-role');
const buildGetAllUsers = require('./get-all');
const buildDeleteUser = require('./delete');
const buildCreateUser = require('./create');
const buildUpdatePassword = require('./update-password');

const comparePasswordAsync = async (password, hashedPassword) => {
  return await bcrypt.compare(password, hashedPassword);
};

module.exports = Object.freeze({
  getUserById: buildGetUserById({userRepository}),
  getUsersByRole: buildGetUsersByRole({userRepository}),
  getAllUsers: buildGetAllUsers({userRepository}),
  deleteUser: buildDeleteUser({userRepository}),
  createUser: buildCreateUser({userRepository}),
  updatePassword: buildUpdatePassword({userRepository, comparePasswordAsync})
});