module.exports = ({userRepository}) => async command => {
  const deleted = await userRepository.deleteUser(command.id);
  if (deleted) {
    return '';
  }

  return `Nepavyko ištrinti vartotojo su id = ${command.id}`;
};