module.exports = ({userRepository}) => async command => {
  const users = await userRepository.getUsersByRole(command.roleId);
  return users.map(user => ({
    id: user.id,
    name: user.name,
    surname: user.surname
  }));
};