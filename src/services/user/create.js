module.exports = ({userRepository}) => async (command) => {
  const user = await userRepository.getUserByLogin(command.login);
  if (user) {
    return {error: `Vartotojas su prisijungimo vardu ${command.login} jau egzistuoja`};
  }

  const createdUser = await userRepository.insertUser(
    command.name,
    command.surname,
    command.login,
    command.password,
    command.roleId);

  return Object.freeze({
    id: createdUser.id,
    name: command.name,
    surname: command.surname,
    roleId: command.roleId
  });
};