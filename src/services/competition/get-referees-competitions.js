module.exports = ({competitionRepository}) => async (query) => {
  const competitions = await competitionRepository.getByChampionshipIdAndRefereeId(query.championshipId, query.refereeId);
  return competitions
    .filter(competition => !competition.finished)
    .map(competition => ({
      id: competition.id,
      name: competition.name,
      resultTypeId: competition.result_type_id,
      active: competition.active
    }));
};