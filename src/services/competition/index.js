const competitionRepository = require('../../repositories/competition');
const teamRepository = require('../../repositories/team');
const resultRepository = require('../../repositories/result');
const buildGetById = require('./get-by-id');
const buildGetByChampionshipId = require('./get-by-championshipId');
const buildGetCoachesCompetitions = require('./get-coaches-competitions');
const buildGetRefereesCompetitions = require('./get-referees-competitions');
const buildGetSoloCompetitors = require('./get-solo-competitors');
const buildGetTeamCompetitors = require('./get-team-competitors');
const buildCreateCompetition = require('./create');
const buildEditCompetition = require('./edit');
const buildActivateCompetition = require('./activate');
const buildFinishSoloCompetition = require('./finish-solo');
const buildFinishTeamCompetition = require('./finish-team');
const buildDeleteCompetition = require('./delete');
const buildRegisterTeam = require('./register-team');
const buildRegisterTeamMember = require('./register-team-member');

module.exports = Object.freeze({
  getById: buildGetById({competitionRepository}),
  getByChampionshipId: buildGetByChampionshipId({competitionRepository}),
  getCoachesCompetitions: buildGetCoachesCompetitions({competitionRepository, teamRepository, resultRepository}),
  getRefereesCompetitions: buildGetRefereesCompetitions({competitionRepository}),
  getSoloCompetitors: buildGetSoloCompetitors({competitionRepository}),
  getTeamCompetitors: buildGetTeamCompetitors({competitionRepository}),
  createCompetition: buildCreateCompetition({competitionRepository}),
  editCompetition: buildEditCompetition({competitionRepository}),
  activateCompetition: buildActivateCompetition({competitionRepository}),
  finishSoloCompetition: buildFinishSoloCompetition({competitionRepository, resultRepository}),
  finishTeamCompetition: buildFinishTeamCompetition({competitionRepository, resultRepository}),
  deleteCompetition: buildDeleteCompetition({competitionRepository}),
  registerTeam: buildRegisterTeam({resultRepository}),
  registerTeamMember: buildRegisterTeamMember({resultRepository})
});
