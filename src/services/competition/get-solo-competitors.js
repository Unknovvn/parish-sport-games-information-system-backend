module.exports = ({competitionRepository}) => async (query) => {
  const competitors = await competitionRepository.getSoloCompetitors(query.competitionId);
  return competitors.map(c => ({
    id: c.id,
    name: c.name,
    surname: c.surname
  }));
};