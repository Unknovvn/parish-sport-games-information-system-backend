module.exports = ({competitionRepository}) => async (query) => {
  const competitions = await competitionRepository.getByChampionshipId(query.championshipId);
  return competitions.map(competition => ({
    id: competition.id,
    name: competition.name,
    refereeId: competition.referee_id,
    resultTypeId: competition.result_type_id,
    active: competition.active,
    finished: competition.finished
  }));
};