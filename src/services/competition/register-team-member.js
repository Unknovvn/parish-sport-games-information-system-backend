module.exports = ({resultRepository}) => async (command) => {
  const createdResult = await resultRepository.insert(
    command.competitionId,
    command.teamId,
    command.teamMemberId);

  return Object.freeze({
    id: createdResult.id,
    competitionId: command.competitionId,
    teamId: command.teamId,
    teamMemeberId: command.teamMemberId
  });
};