module.exports = ({competitionRepository, teamRepository, resultRepository}) => async (query) => {
  const competitions = await competitionRepository.getByChampionshipId(query.championshipId);
  const team = await teamRepository.getByChampionshipIdAndCoachId(query.championshipId, query.coachId);
  if (!team) {
    return [];
  }

  return await Promise.all(competitions.map(async c => {
    const results = await resultRepository.getByCompetitionIdAndTeamId(c.id, team.id);
    return {
      id: c.id,
      name: c.name,
      resultTypeId: c.result_type_id,
      active: c.active,
      finished: c.finished,
      isRegistered: results && results.length > 0
    };
  }));
};