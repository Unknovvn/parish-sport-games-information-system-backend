module.exports = ({resultRepository}) => async (command) => {
  const createdResult = await resultRepository.insert(
    command.competitionId,
    command.teamId,
    null);

  return Object.freeze({
    id: createdResult.id,
    competitionId: command.competitionId,
    teamId: command.teamId,
  });
};