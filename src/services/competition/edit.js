module.exports = ({competitionRepository}) => async (query, command) => {
  return await competitionRepository.updateCompetition(
    command.name,
    command.refereeId,
    command.resultTypeId,
    query.id);
};