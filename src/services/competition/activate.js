module.exports = ({competitionRepository}) => async query => {
  return await competitionRepository.activateCompetition(query.id);
};