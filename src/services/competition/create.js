module.exports = ({competitionRepository}) => async (command) => {
  const existingCompetition = await competitionRepository.getByChampionshipIdAndName(command.championshipId, command.name);
  if (existingCompetition)
    return {error: `Šiame čempionate varžybos su pavadinimu ${command.name} jau egzistuoja`};

  const createdCompetition = await competitionRepository.insertCompetition(
    command.name,
    command.refereeId,
    command.championshipId,
    command.resultTypeId,
    false,
    false);

  return Object.freeze({
    id: createdCompetition.id,
    name: command.name,
    refereeId: command.refereeId,
    championshipId: command.championshipId,
    resultTypeId: command.resultTypeId
  });
};