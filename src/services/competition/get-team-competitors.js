module.exports = ({competitionRepository}) => async (query) => {
  const competitors = await competitionRepository.getTeamCompetitors(query.competitionId);
  return competitors.map(c => ({
    id: c.id,
    name: c.name
  }));
};