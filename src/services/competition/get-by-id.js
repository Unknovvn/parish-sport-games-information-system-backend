module.exports = ({competitionRepository}) => async (query) => {
  const competition = await competitionRepository.getById(query.id);
  if (competition) {
    return {
      id: competition.id,
      name: competition.name,
      resultTypeId: competition.result_type_id
    }
  }

  return null;
};