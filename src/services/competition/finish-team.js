module.exports = ({competitionRepository, resultRepository}) => async (query, command) => {
  await competitionRepository.finishCompetition(query.id);
  for (const r of command.results) {
    await resultRepository.updateTeam(query.id, r.competitorId, r.place);
  }

  return true;
};