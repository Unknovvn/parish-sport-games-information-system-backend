module.exports = ({competitionRepository}) => async command => {
  const deleted = await competitionRepository.deleteCompetition(command.id);
  if (deleted) {
    return '';
  }

  return `Nepavyko ištrinti varžybų su id = ${command.id}`;
};