module.exports = ({teamRepository, teamMembersRepository}) => async (query, command) => {
  const team = await teamRepository.getById(query.id);
  if (!team) {
    return;
  }

  if (team.name !== command.name) {
    await teamRepository.updateName(query.id, command.name);
  }

  let teamMembers = await teamMembersRepository.getByTeamId(query.id);
  for (const member of command.members) {
    const teamMember = teamMembers.find(tm => tm.id === member.id);
    if (teamMember) {
      teamMembers = teamMembers.filter(m => m.id !== teamMember.id);
      if (teamMember.name !== member.name || teamMember.surname !== member.surname) {
        await teamMembersRepository.update(member.id, member.name, member.surname);
      }
    } else {
      await teamMembersRepository.insert(member.name, member.surname, query.id);
    }
  }

  for (const member of teamMembers) {
    await teamMembersRepository.delete(member.id);
  }

  return true;
};