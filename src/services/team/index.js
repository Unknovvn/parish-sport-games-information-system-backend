const teamRepository = require('../../repositories/team');
const teamMembersRepository = require('../../repositories/team-members');
const buildGetByChampionshipIdAndCoachId = require('./get-by-championship-id-and-coach-id');
const buildCreate = require('./create');
const buildUpdateName = require('./update')
const buildDelete = require('./delete');

module.exports = Object.freeze({
  getByChampionshipIdAndCoachId: buildGetByChampionshipIdAndCoachId({teamRepository, teamMembersRepository}),
  create: buildCreate({teamRepository, teamMembersRepository}),
  update: buildUpdateName({teamRepository, teamMembersRepository}),
  delete: buildDelete({teamRepository})
});
