module.exports = ({teamRepository}) => async command => {
  const deleted = await teamRepository.delete(command.id);
  if (deleted) {
    return '';
  }

  return `Nepavyko ištrinti komandos su id = ${command.id}`;
};