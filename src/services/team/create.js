module.exports = ({teamRepository, teamMembersRepository}) => async (command) => {
  const team = await teamRepository.getByChampionshipIdAndCoachId(
    command.championshipId,
    command.coachId);

  if (team) {
    return {error: `Komanda jau yra užregistruota`};
  }

  const createdTeam = await teamRepository.insert(
    command.name,
    command.coachId,
    command.championshipId);

  for (const member of command.members) {
    await teamMembersRepository.insert(member.name, member.surname, createdTeam.id);
  }

  return Object.freeze({
    id: createdTeam.id,
    name: command.name,
    members: command.members,
    coachId: command.coachId,
    championshipId: command.championshipId
  });
};