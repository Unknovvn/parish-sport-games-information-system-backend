module.exports = ({teamRepository, teamMembersRepository}) => async query => {
  const team = await teamRepository.getByChampionshipIdAndCoachId(query.championshipId, query.coachId);
  if (team) {
    const teamMembers = await teamMembersRepository.getByTeamId(team.id);

    return {
      id: team.id,
      name: team.name,
      members: teamMembers
    };
  }

  return null;
};