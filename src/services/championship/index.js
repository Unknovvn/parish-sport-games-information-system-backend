const championshipRepository = require('../../repositories/championship');
const competitionRepository = require('../../repositories/competition');
const teamRepository = require('../../repositories/team');
const resultRepository = require('../../repositories/result');
const buildGetAll = require('./get-all');
const buildGetCoaches = require('./get-coaches-championships');
const buildGetActive = require('./get-active');
const buildGetFinished = require('./get-finished');
const buildGetResults = require('./get-results');
const buildCreate = require('./create');
const buildDelete = require('./delete');
const buildActivate = require('./activate');
const buildFinish = require('./finish');
const buildEdit = require('./edit');

module.exports = Object.freeze({
  get: buildGetAll({championshipRepository}),
  getCoachesChampionships: buildGetCoaches({championshipRepository}),
  getActive: buildGetActive({championshipRepository}),
  getFinished: buildGetFinished({championshipRepository}),
  getResults: buildGetResults({resultRepository, competitionRepository, teamRepository}),
  create: buildCreate({championshipRepository}),
  delete: buildDelete({championshipRepository}),
  activate: buildActivate({championshipRepository}),
  finish: buildFinish({championshipRepository}),
  edit: buildEdit({championshipRepository})
});