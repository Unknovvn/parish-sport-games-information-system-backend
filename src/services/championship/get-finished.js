module.exports = ({championshipRepository}) => async () => {
  return await championshipRepository.getByActiveAndFinished(false, true);
};