module.exports = ({championshipRepository}) => async query => {
  return await championshipRepository.finishChampionship(query.id);
};