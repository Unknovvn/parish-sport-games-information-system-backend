module.exports = ({championshipRepository}) => async (query, command) => {
  return await championshipRepository.updateChampionship(command.name, command.date, query.id);
};