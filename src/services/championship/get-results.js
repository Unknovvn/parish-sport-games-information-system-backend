module.exports = ({resultRepository, teamRepository}) => async query => {
  const results = await resultRepository.getFinishedByChampionshipId(query.id);
  const soloResults = results.filter(r => r.result);
  const teamResults = results.filter(r => r.place);

  const teamPoints = new Map();
  const groupedSoloResults = groupBy(soloResults, sr => sr.competition_id);
  for (const k of groupedSoloResults.keys()) {
    const competitionResults = groupedSoloResults.get(k);
    const sortedCompetitionResults = competitionResults.sort(cr => cr.result);
    for (const scr of sortedCompetitionResults) {
      const competitionPoints = getSoloResultPoints(sortedCompetitionResults.indexOf(scr) + 1);
      const key = scr.team_id;
      const points = teamPoints.get(key);
      if (points) {
        teamPoints.set(key, points + competitionPoints);
      } else {
        teamPoints.set(key, competitionPoints);
      }
    }
  }

  for (const tr of teamResults) {
    const competitionPoints = getTeamResultPoints(tr.place);
    const key = tr.team_id;
    const points = teamPoints.get(key);
    if (points) {
      teamPoints.set(key, points + competitionPoints);
    } else {
      teamPoints.set(key, competitionPoints);
    }
  }

  return await Promise.all([...teamPoints.keys()].map(async k => {
    const team = await teamRepository.getById(k);
    return {
      teamName: team.name,
      points: teamPoints.get(k)
    };
  }));
};

const getTeamResultPoints = place => {
  switch (place) {
    case 1:
      return 10;
    case 2:
      return 7;
    case 3:
      return 5;
    default:
      return 0;
  }
}

const getSoloResultPoints = place => {
  switch (place) {
    case 1:
      return 5;
    case 2:
      return 2;
    case 3:
      return 1;
    default:
      return 0;
  }
}

const groupBy = (list, keyGetter) => {
  const map = new Map();
  list.forEach(item => {
    const key = keyGetter(item);
    const collection = map.get(key);
    if (!collection) {
      map.set(key, [item]);
    } else {
      collection.push(item);
    }
  })

  return map;
};
