module.exports = ({championshipRepository}) => async command => {
  const deleted = await championshipRepository.deleteChampionship(command.id);
  if (deleted) {
    return '';
  }

  return `Nepavyko ištrinti čempionato su id = ${command.id}`;
};