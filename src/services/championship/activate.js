module.exports = ({championshipRepository}) => async query => {
  return await championshipRepository.activateChampionship(query.id);
};