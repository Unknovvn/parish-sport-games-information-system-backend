module.exports = ({championshipRepository}) => async (command) => {
  const championship = await championshipRepository.getChampionshipByName(command.name);
  if (championship) {
    return {error: `Čempionatas su pavadinimu ${command.name} jau egzistuoja`};
  }

  const createdChampionship = await championshipRepository.insertChampionship(
    command.name,
    command.date,
    false,
    false);

  return Object.freeze({
    id: createdChampionship.id,
    name: command.name,
    date: command.date
  });
};