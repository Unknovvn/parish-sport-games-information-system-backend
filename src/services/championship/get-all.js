module.exports = ({championshipRepository}) => async () => {
  const users = await championshipRepository.getAllChampionships();
  return users.map(championship => ({
    id: championship.id,
    name: championship.name,
    date: championship.date,
    active: championship.active,
    finished: championship.finished
  }));
};