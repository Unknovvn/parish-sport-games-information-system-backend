module.exports = ({championshipRepository}) => async () => {
  return await championshipRepository.getByActiveAndFinished(true, false);
};