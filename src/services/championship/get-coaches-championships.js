module.exports = ({championshipRepository}) => async query => {
  const preparedChampionships = await championshipRepository.getByActiveAndFinished(false, false);
  const coachesChampionships = await championshipRepository.getCoachesChampionships(query.coachId);

  return [
    ...preparedChampionships
      .filter(c => coachesChampionships.filter(cc => cc.id === c.id).length === 0)
      .map(c => ({
      id: c.id,
      name: c.name,
      date: c.date,
      active: c.active,
      finished: c.finished,
      isRegistered: false
    })),
    ...coachesChampionships.map(c => ({
      id: c.id,
      name: c.name,
      date: c.date,
      active: c.active,
      finished: c.finished,
      isRegistered: true
    }))
  ];
};