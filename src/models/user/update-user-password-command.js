module.exports = ({hashPassword, validationMethod}) => body => {
  const validationErrors = validationMethod(body);
  return validationErrors
    ? {validationErrors}
    : {
      password: body.password,
      newPassword: hashPassword(body.newPassword)
    }
};