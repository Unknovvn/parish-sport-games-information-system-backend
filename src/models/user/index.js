const bcrypt = require('bcrypt');
const Joi = require('@hapi/joi');
const UserRoles = require('../../constants/user-roles');
const validate = require('../../utils/validator');
const buildByRoleIdQuery = require('./by-roleId-query');
const buildCreateUserCommand = require('./create-user-command');
const buildUpdateUserPasswordCommand = require('./update-user-password-command');

const hashPassword = (password) => {
  const SALT_COMPLEXITY = 11;
  const salt = bcrypt.genSaltSync(SALT_COMPLEXITY);
  return bcrypt.hashSync(password, salt);
};

const queryValidationSchema = Joi.object({
  roleId: Joi.number().required().integer().valid(...Object.values(UserRoles)).messages({
    'any.required': 'Rolės id yra privalomas laukas',
    'any.only': 'Neteisinga rolės id reikšmė',
    'number.base': 'Neteisinga rolės id reikšmė',
    'number.integer': 'Neteisinga rolės id reikšmė'
  })
});

const createUserValidationSchema = Joi.object({
  name: Joi.string().required().empty().max(100).messages({
    'any.required': 'Vardas yra privalomas laukas',
    'string.empty': 'Vardas privalo turėti reikšmę',
    'string.max': 'Vardas privalo būti trumpesnis nei 100 simbolių'
  }),
  surname: Joi.string().required().empty().max(100).messages({
    'any.required': 'Pavardė yra privalomas laukas',
    'string.empty': 'Pavardė privalo turėti reikšmę',
    'string.max': 'Pavardė privalo būti trumpesnis nei 100 simbolių'
  }),
  login: Joi.string().required().empty().max(50).messages({
    'any.required': 'Vartotojo vardas yra privalomas laukas',
    'string.empty': 'Vartotojo vardas privalo turėti reikšmę',
    'string.max': 'Vartotojo vardas privalo būti trumpesnis nei 50 simbolių'
  }),
  password: Joi.string().required().empty().max(50).messages({
    'any.required': 'Slaptažodis yra privalomas laukas',
    'string.empty': 'Slaptažodis privalo turėti reikšmę',
    'string.max': 'Slaptažodis privalo būti trumpesnis nei 50 simbolių'
  }),
  roleId: Joi.number().required().integer().valid(...Object.values(UserRoles)).messages({
    'any.required': 'Rolės id yra privalomas laukas',
    'any.only': 'Neteisinga rolės id reikšmė',
    'number.base': 'Neteisinga rolės id reikšmė',
    'number.integer': 'Neteisinga rolės id reikšmė'
  })
});

const updateUserPasswordValidationSchema = Joi.object({
  password: Joi.string().required().empty().max(50).messages({
    'any.required': 'Slaptažodis yra privalomas laukas',
    'string.empty': 'Slaptažodis privalo turėti reikšmę',
    'string.max': 'Slaptažodis privalo būti trumpesnis nei 50 simbolių'
  }),
  newPassword: Joi.string().required().empty().max(50).messages({
    'any.required': 'Naujas slaptažodis yra privalomas laukas',
    'string.empty': 'Naujas slaptažodis privalo turėti reikšmę',
    'string.max': 'Naujas slaptažodis privalo būti trumpesnis nei 50 simbolių'
  })
});

module.exports = Object.freeze({
  byRoleIdQuery: buildByRoleIdQuery({
    validationMethod: (body) => validate(queryValidationSchema, body)
  }),
  createUserCommand: buildCreateUserCommand({
    hashPassword,
    validationMethod: (body) => validate(createUserValidationSchema, body)
  }),
  updateUserPasswordCommand: buildUpdateUserPasswordCommand({
    hashPassword,
    validationMethod: (body) => validate(updateUserPasswordValidationSchema, body)
  })
});