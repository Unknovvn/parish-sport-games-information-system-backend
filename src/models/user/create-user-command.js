module.exports = ({hashPassword, validationMethod}) => body => {
  const validationErrors = validationMethod(body);
  return validationErrors
    ? {validationErrors}
    : {
      name: body.name,
      surname: body.surname,
      login: body.login,
      password: hashPassword(body.password),
      roleId: body.roleId
    }
};