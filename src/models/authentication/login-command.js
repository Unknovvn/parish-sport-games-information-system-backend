module.exports = ({validationMethod}) => {
  return (body) => {
    const validationErrors = validationMethod(body);
    return validationErrors
      ? {validationErrors}
      : {
        username: body.username,
        password: body.password,
      };
  };
};