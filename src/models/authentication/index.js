const Joi = require('@hapi/joi');
const validate = require('../../utils/validator');
const buildLoginCommand = require('./login-command');

const validationSchema = Joi.object({
  username: Joi.string().required().empty().max(50).messages({
    'any.required': 'Prisijungimo vardas yra privalomas laukas',
    'string.empty': 'Prisijungimo vardas privalo turėti reikšmę',
    'string.max': 'Prisijungimo vardas privalo būti trumpesnis nei 50 simbolių'
  }),
  password: Joi.string().required().empty().max(50).messages({
    'any.required': 'Slaptažodis yra privalomas laukas',
    'string.empty': 'Slaptažodis privalo turėti reikšmę',
    'string.max': 'Slaptažodis privalo būti trumpesnis nei 50 simbolių'
  })
});

module.exports = Object.freeze({
  loginCommand: buildLoginCommand({
    validationMethod: (body) => validate(validationSchema, body)
  })
});