const Joi = require('@hapi/joi');
const validate = require('../../utils/validator');
const buildByChampionshipIdAndCoachIdQuery = require('./by-championshipId-and-coachId-query');
const buildCreateTeamCommand = require('./create-command');
const buildEditTeamCommand = require('./edit-command');

const queryValidationSchema = Joi.object({
  championshipId: Joi.number().required().integer().messages({
    'any.required': 'Čempionato id yra privalomas laukas',
    'number.base': 'Neteisinga čempionato id reikšmė',
    'number.integer': 'Neteisinga čempionato id reikšmė'
  }),
  coachId: Joi.number().required().integer().messages({
    'any.required': 'Trenerio id yra privalomas laukas',
    'number.base': 'Neteisinga trenerio id reikšmė',
    'number.integer': 'Neteisinga trenerio id reikšmė'
  }),
});

const createTeamValidationSchema = Joi.object({
  name: Joi.string().required().empty().max(100).messages({
    'any.required': 'Pavadinimas yra privalomas laukas',
    'string.empty': 'Pavadinimas privalo turėti reikšmę',
    'string.max': 'Pavadinimas privalo būti trumpesnis nei 100 simbolių'
  }),
  members: Joi.array().required().min(1).messages({
    'any.required': 'Komandos nariai yra privalomas laukas',
    'array.base': 'Neleistina komandos narių reikšmė',
    'array.min': 'Komanda privalo turėti bent vieną komandos narį'
  }),
  coachId: Joi.number().required().integer().messages({
    'any.required': 'Trenerio id yra privalomas laukas',
    'number.base': 'Neteisinga trenerio id reikšmė',
    'number.integer': 'Neteisinga trenerio id reikšmė'
  }),
  championshipId: Joi.number().required().integer().messages({
    'any.required': 'Čempionato id yra privalomas laukas',
    'number.base': 'Neteisinga čempionato id reikšmė',
    'number.integer': 'Neteisinga čempionato id reikšmė'
  })
});

const editTeamValidationSchema = Joi.object({
  name: Joi.string().required().empty().max(100).messages({
    'any.required': 'Pavadinimas yra privalomas laukas',
    'string.empty': 'Pavadinimas privalo turėti reikšmę',
    'string.max': 'Pavadinimas privalo būti trumpesnis nei 100 simbolių'
  }),
  members: Joi.array().required().min(1).messages({
    'any.required': 'Komandos nariai yra privalomas laukas',
    'array.base': 'Neleistina komandos narių reikšmė',
    'array.min': 'Komanda privalo turėti bent vieną komandos narį'
  }),
})

module.exports = Object.freeze({
  byChampionshipIdAndCoachIdQuery: buildByChampionshipIdAndCoachIdQuery({
    validationMethod: (body) => validate(queryValidationSchema, body)
  }),
  createTeamCommand: buildCreateTeamCommand({
    validationMethod: (body) => validate(createTeamValidationSchema, body)
  }),
  editTeamCommand: buildEditTeamCommand({
    validationMethod: (body) => validate(editTeamValidationSchema, body)
  })
})