const Joi = require('@hapi/joi');
const validate = require('../../utils/validator');
const buildByIdQuery = require('./by-id-query');

const validationSchema = Joi.object({
  id: Joi.number().required().integer().messages({
    'any.required': 'Id yra privalomas laukas',
    'number.base': 'Neteisinga id reikšmė',
    'number.integer': 'Neteisinga id reikšmė'
  })
});

module.exports = Object.freeze({
  byIdQuery: buildByIdQuery({
    validationMethod: (body) => validate(validationSchema, body)
  })
});