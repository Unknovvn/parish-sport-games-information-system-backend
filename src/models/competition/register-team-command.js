module.exports = ({validationMethod}) => body => {
  const validationErrors = validationMethod(body);
  return validationErrors
    ? {validationErrors}
    : {
      competitionId: body.competitionId,
      teamId: body.teamId
    }
};