module.exports = ({validationMethod}) => {
  return (params) => {
    const validationErrors = validationMethod(params);
    return validationErrors
      ? {validationErrors}
      : {competitionId: params.competitionId}
  };
};