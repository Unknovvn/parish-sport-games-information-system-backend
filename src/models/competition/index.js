const Joi = require('@hapi/joi');
const ResultTypes = require('../../constants/result-types');
const validate = require('../../utils/validator');
const buildByChampionshipIdQuery = require('./by-championshipId-query');
const buildByCompetitionIdQuery = require('./by-competitionId-query');
const buildByChampionshipIdAndCoachIdQuery = require('./by-championshipId-and-coachId-query');
const buildByChampionshipIdAndRefereeIdQuery = require('./by-championshipId-and-refereeId-query');
const buildCreateCompetitionCommand = require('./create-competition-command');
const buildEditCompetitionCommand = require('./edit-competition-command');
const buildRegisterTeamCommand = require('./register-team-command');
const buildRegisterTeamMemberCommand = require('./register-team-member-command');
const buildFinishCompetitionCommand = require('./finish-competition-command');

const byChampionshipIdQueryValidationSchema = Joi.object({
  championshipId: Joi.number().required().integer().messages({
    'any.required': 'Čempionato id yra privalomas laukas',
    'number.base': 'Neteisinga čempionato id reikšmė',
    'number.integer': 'Neteisinga čempionato id reikšmė'
  })
});

const byCompetitionIdQueryValidationSchema = Joi.object({
  competitionId: Joi.number().required().integer().messages({
    'any.required': 'Varžybų id yra privalomas laukas',
    'number.base': 'Neteisinga varžybų id reikšmė',
    'number.integer': 'Neteisinga varžybų id reikšmė'
  })
});

const byChampionshipIdAndCoachIdValidationShema = Joi.object({
  championshipId: Joi.number().required().integer().messages({
    'any.required': 'Čempionato id yra privalomas laukas',
    'number.base': 'Neteisinga čempionato id reikšmė',
    'number.integer': 'Neteisinga čempionato id reikšmė'
  }),
  coachId: Joi.number().required().integer().messages({
    'any.required': 'Trenerio id yra privalomas laukas',
    'number.base': 'Neteisinga trenerio id reikšmė',
    'number.integer': 'Neteisinga trenerio id reikšmė'
  })
});

const byChampionshipIdAndRefereeIdValidationShema = Joi.object({
  championshipId: Joi.number().required().integer().messages({
    'any.required': 'Čempionato id yra privalomas laukas',
    'number.base': 'Neteisinga čempionato id reikšmė',
    'number.integer': 'Neteisinga čempionato id reikšmė'
  }),
  refereeId: Joi.number().required().integer().messages({
    'any.required': 'Teisėjo id yra privalomas laukas',
    'number.base': 'Neteisinga teisėjo id reikšmė',
    'number.integer': 'Neteisinga teisėjo id reikšmė'
  })
});

const createCompetitionValidationSchema = Joi.object({
  name: Joi.string().required().empty().max(100).messages({
    'any.required': 'Pavadinimas yra privalomas laukas',
    'string.empty': 'Pavadinimas privalo turėti reikšmę',
    'string.max': 'Pavadinimas privalo būti trumpesnis nei 100 simbolių'
  }),
  refereeId: Joi.number().required().integer().messages({
    'any.required': 'Teisėjo id yra privalomas laukas',
    'number.base': 'Neteisinga teisėjo id reikšmė',
    'number.integer': 'Neteisinga teisėjo id reikšmė'
  }),
  championshipId: Joi.number().required().integer().messages({
    'any.required': 'Čempionato id yra privalomas laukas',
    'number.base': 'Neteisinga čempionato id reikšmė',
    'number.integer': 'Neteisinga čempionato id reikšmė'
  }),
  resultTypeId: Joi.number().required().integer().valid(...Object.values(ResultTypes)).messages({
    'any.required': 'Rezultato tipo id yra privalomas laukas',
    'any.only': 'Neteisinga Rezultato tipo id reikšmė',
    'number.base': 'Neteisinga Rezultato tipo id reikšmė',
    'number.integer': 'Neteisinga Rezultato tipo id reikšmė'
  })
});

const editCompetitionValidationSchema = Joi.object({
  name: Joi.string().required().empty().max(100).messages({
    'any.required': 'Pavadinimas yra privalomas laukas',
    'string.empty': 'Pavadinimas privalo turėti reikšmę',
    'string.max': 'Pavadinimas privalo būti trumpesnis nei 100 simbolių'
  }),
  refereeId: Joi.number().required().integer().messages({
    'any.required': 'Teisėjo id yra privalomas laukas',
    'number.base': 'Neteisinga teisėjo id reikšmė',
    'number.integer': 'Neteisinga teisėjo id reikšmė'
  }),
  resultTypeId: Joi.number().required().integer().valid(...Object.values(ResultTypes)).messages({
    'any.required': 'Rezultato tipo id yra privalomas laukas',
    'any.only': 'Neteisinga Rezultato tipo id reikšmė',
    'number.base': 'Neteisinga Rezultato tipo id reikšmė',
    'number.integer': 'Neteisinga Rezultato tipo id reikšmė'
  })
});

const registerTeamValidationSchema = Joi.object({
  competitionId: Joi.number().required().integer().messages({
    'any.required': 'Varžybų id yra privalomas laukas',
    'number.base': 'Neteisinga varžybų id reikšmė',
    'number.integer': 'Neteisinga varžybų id reikšmė'
  }),
  teamId: Joi.number().required().integer().messages({
    'any.required': 'Komandos id yra privalomas laukas',
    'number.base': 'Neteisinga komandos id reikšmė',
    'number.integer': 'Neteisinga komandos id reikšmė'
  })
});

const registerTeamMemberValidationSchema = Joi.object({
  competitionId: Joi.number().required().integer().messages({
    'any.required': 'Varžybų id yra privalomas laukas',
    'number.base': 'Neteisinga varžybų id reikšmė',
    'number.integer': 'Neteisinga varžybų id reikšmė'
  }),
  teamId: Joi.number().required().integer().messages({
    'any.required': 'Komandos id yra privalomas laukas',
    'number.base': 'Neteisinga komandos id reikšmė',
    'number.integer': 'Neteisinga komandos id reikšmė'
  }),
  teamMemberId: Joi.number().required().integer().messages({
    'any.required': 'Komandos nario id yra privalomas laukas',
    'number.base': 'Neteisinga komandos nario id reikšmė',
    'number.integer': 'Neteisinga komandos nario id reikšmė'
  })
});

const finishCompetitionValidationSchema = Joi.object({
  results: Joi.array().required().min(1).messages({
    'any.required': 'Rezultatai yra privalomas laukas',
    'array.base': 'Neleistina rezultatų reikšmė',
    'array.min': 'Varžybos privalo turėti bent vieną rezultatą'
  })
})

module.exports = Object.freeze({
  byChampionshipIdQuery: buildByChampionshipIdQuery({
    validationMethod: (body) => validate(byChampionshipIdQueryValidationSchema, body)
  }),
  byCompetitionIdQuery: buildByCompetitionIdQuery({
    validationMethod: (body) => validate(byCompetitionIdQueryValidationSchema, body)
  }),
  byChampionshipIdAndCoachIdQuery: buildByChampionshipIdAndCoachIdQuery({
    validationMethod: (body) => validate(byChampionshipIdAndCoachIdValidationShema, body)
  }),
  byChampionshipIdAndRefereeIdQuery: buildByChampionshipIdAndRefereeIdQuery({
    validationMethod: (body) => validate(byChampionshipIdAndRefereeIdValidationShema, body)
  }),
  createCompetitionCommand: buildCreateCompetitionCommand({
    validationMethod: (body) => validate(createCompetitionValidationSchema, body)
  }),
  editCompetitionCommand: buildEditCompetitionCommand({
    validationMethod: (body) => validate(editCompetitionValidationSchema, body)
  }),
  registerTeamCommand: buildRegisterTeamCommand({
    validationMethod: (body) => validate(registerTeamValidationSchema, body)
  }),
  registerTeamMemberCommand: buildRegisterTeamMemberCommand({
    validationMethod: (body) => validate(registerTeamMemberValidationSchema, body)
  }),
  finishCompetitionCommand: buildFinishCompetitionCommand({
    validationMethod: (body) => validate(finishCompetitionValidationSchema, body)
  })
});