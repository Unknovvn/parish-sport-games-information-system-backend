module.exports = ({validationMethod}) => (body) => {
  const validationErrors = validationMethod(body);
  return validationErrors
    ? {validationErrors}
    : {
      name: body.name,
      refereeId: body.refereeId,
      championshipId: body.championshipId,
      resultTypeId: body.resultTypeId
    }
};