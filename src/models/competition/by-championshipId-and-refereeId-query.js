module.exports = ({validationMethod}) => {
  return (params) => {
    const validationErrors = validationMethod(params);
    return validationErrors
      ? {validationErrors}
      : {
        championshipId: params.championshipId,
        refereeId: params.refereeId
      }
  };
};