module.exports = ({validationMethod}) => body => {
  const validationErrors = validationMethod(body);
  return validationErrors
    ? {validationErrors}
    : {results: body.results}
};