module.exports = ({validationMethod}) => {
  return (params) => {
    const validationErrors = validationMethod(params);
    return validationErrors
      ? {validationErrors}
      : {teamId: params.teamId}
  };
};