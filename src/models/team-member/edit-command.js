module.exports = ({validationMethod}) => body => {
  const validationErrors = validationMethod(body);
  return validationErrors
    ? {validationErrors}
    : {
      name: body.name,
      surname: body.surname
    }
};