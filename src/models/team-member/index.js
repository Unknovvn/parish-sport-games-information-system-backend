const Joi = require('@hapi/joi');
const validate = require('../../utils/validator');
const buildByTeamIdQuery = require('./by-teamId-query');
const buildCreateCommand = require('./create-command');
const buildEditCommand = require('./edit-command');

const byTeamIdQueryValidationSchema = Joi.object({
  teamId: Joi.number().required().integer().messages({
    'any.required': 'Komandos id yra privalomas laukas',
    'number.base': 'Neteisinga komandos id reikšmė',
    'number.integer': 'Neteisinga komandos id reikšmė'
  })
});

const createCommandValidationSchema = Joi.object({
  name: Joi.string().required().empty().max(100).messages({
    'any.required': 'Vardas yra privalomas laukas',
    'string.empty': 'Vardas privalo turėti reikšmę',
    'string.max': 'Vardas privalo būti trumpesnis nei 100 simbolių'
  }),
  surname: Joi.string().required().empty().max(100).messages({
    'any.required': 'Pavardė yra privalomas laukas',
    'string.empty': 'Pavardė privalo turėti reikšmę',
    'string.max': 'Pavardė privalo būti trumpesnis nei 100 simbolių'
  }),
  teamId: Joi.number().required().integer().messages({
    'any.required': 'Komandos id yra privalomas laukas',
    'number.base': 'Neteisinga komandos id reikšmė',
    'number.integer': 'Neteisinga komandos id reikšmė'
  })
});

const editCommandValidationSchema = Joi.object({
  name: Joi.string().required().empty().max(100).messages({
    'any.required': 'Vardas yra privalomas laukas',
    'string.empty': 'Vardas privalo turėti reikšmę',
    'string.max': 'Vardas privalo būti trumpesnis nei 100 simbolių'
  }),
  surname: Joi.string().required().empty().max(100).messages({
    'any.required': 'Pavardė yra privalomas laukas',
    'string.empty': 'Pavardė privalo turėti reikšmę',
    'string.max': 'Pavardė privalo būti trumpesnis nei 100 simbolių'
  })
});

module.exports = Object.freeze({
  byTeamIdQuery: buildByTeamIdQuery({
    validationMethod: (body) => validate(byTeamIdQueryValidationSchema, body)
  }),
  createCommand: buildCreateCommand({
    validationMethod: (body) => validate(createCommandValidationSchema, body)
  }),
  editCommand: buildEditCommand({
    validationMethod: (body) => validate(editCommandValidationSchema, body)
  })
});