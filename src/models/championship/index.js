const Joi = require('@hapi/joi');
const validate = require('../../utils/validator');
const buildByCoachIdQuery = require('./by-coachId-query');
const buildCreateChampionshipCommand = require('./create-championship-command');
const buildEditChampionshipCommand = require('./edit-championship-command');

const byCoachIdQueryValidationSchema = Joi.object({
  coachId: Joi.number().required().integer().messages({
    'any.required': 'Trenerio id yra privalomas laukas',
    'number.base': 'Neteisinga trenerio id reikšmė',
    'number.integer': 'Neteisinga trenerio id reikšmė'
  })
});

const validationSchema = Joi.object({
  name: Joi.string().required().empty().max(100).messages({
    'any.required': 'Pavadinimas yra privalomas laukas',
    'string.empty': 'Pavadinimas privalo turėti reikšmę',
    'string.max': 'Pavadinimas privalo būti trumpesnis nei 100 simbolių'
  }),
  date: Joi.date().required().messages({
    'any.required': 'Data yra privalomas laukas',
    'date.base': 'Data turi atitikti datos formatą',
  })
});

module.exports = Object.freeze({
  byCoachIdQuery: buildByCoachIdQuery({
    validationMethod: (body) => validate(byCoachIdQueryValidationSchema, body)
  }),
  createChampionshipCommand: buildCreateChampionshipCommand({
    validationMethod: (body) => validate(validationSchema, body)
  }),
  editChampionshipCommand: buildEditChampionshipCommand({
    validationMethod: (body) => validate(validationSchema, body)
  })
});