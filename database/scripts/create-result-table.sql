CREATE TABLE results
(
    id SERIAL PRIMARY KEY,
    competition_id integer NOT NULL,
    team_id integer NOT NULL,
    team_member_id integer,
    place SMALLINT,
    result VARCHAR(50)
);