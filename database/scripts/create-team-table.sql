CREATE TABLE teams
(
    id SERIAL PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    coach_id integer NOT NULL,
    championship_id integer NOT NULL,
    UNIQUE(coach_id, championship_id)
);