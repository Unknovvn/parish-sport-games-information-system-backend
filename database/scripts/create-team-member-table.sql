CREATE TABLE team_members
(
    id SERIAL PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    surname VARCHAR(100) NOT NULL,
    team_id integer NOT NULL,
    UNIQUE(name, surname, team_id)
);