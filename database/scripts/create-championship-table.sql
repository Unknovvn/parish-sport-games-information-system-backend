CREATE TABLE championships
(
    id SERIAL PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    date DATE NOT NULL,
    active BOOL NOT NULL,
    finished BOOL NOT NULL,
    UNIQUE(name)
);