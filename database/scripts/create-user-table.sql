CREATE TABLE users
(
    id SERIAL PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    surname VARCHAR(100) NOT NULL,
    login VARCHAR(50) NOT NULL,
    password VARCHAR(60) NOT NULL,
    role_id SMALLINT NOT NULL,
    UNIQUE(login)
);
