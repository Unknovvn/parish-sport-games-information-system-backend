CREATE TABLE competitions
(
    id SERIAL PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    referee_id INTEGER NOT NULL,
    championship_id INTEGER NOT NULL,
    result_type_id INTEGER NOT NULL,
    active BOOL NOT NULL,
    finished BOOL NOT NULL,
    UNIQUE(championship_id, name)
);