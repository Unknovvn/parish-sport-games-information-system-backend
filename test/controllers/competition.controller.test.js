const mockRequest = require('../mockRequest');
const mockResponse = require('../mockResponse');
const buildCompetitionController = require('../../src/controllers/competition.controller');

it('getById returns response with status 400 and Id yra privalomas laukas message', async () => {
  // Assert
  const request = mockRequest({}, {});
  const response = mockResponse();
  const fakeServiceResult = [{
    "id": 18,
    "name": "TEAM",
    "refereeId": 61,
    "resultTypeId": 1,
    "active": false,
    "finished": true
  }];

  const competitionService = {getById: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildCompetitionController({competitionService});

  // Act
  await controller.getById(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Id yra privalomas laukas']});
})

it('getById returns response with status 200 and service result as body', async () => {
  // Assert
  const request = mockRequest({}, {id: 1});
  const response = mockResponse();
  const fakeServiceResult = [{
    "id": 18,
    "name": "TEAM",
    "refereeId": 61,
    "resultTypeId": 1,
    "active": false,
    "finished": true
  }];

  const competitionService = {getById: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildCompetitionController({competitionService});

  // Act
  await controller.getById(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(200);
  expect(response.json).toHaveBeenCalledWith(fakeServiceResult);
})

it('getByChampionship returns response with status 400 and Čempionato id yra privalomas laukas message', async () => {
  // Assert
  const request = mockRequest({}, {});
  const response = mockResponse();
  const fakeServiceResult = [{
    "id": 18,
    "name": "TEAM",
    "refereeId": 61,
    "resultTypeId": 1,
    "active": false,
    "finished": true
  }];

  const competitionService = {getByChampionshipId: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildCompetitionController({competitionService});

  // Act
  await controller.getByChampionship(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Čempionato id yra privalomas laukas']});
})

it('getByChampionship returns response with status 200 and service result as body', async () => {
  // Assert
  const request = mockRequest({}, {championshipId: 1});
  const response = mockResponse();
  const fakeServiceResult = [{
    "id": 18,
    "name": "TEAM",
    "refereeId": 61,
    "resultTypeId": 1,
    "active": false,
    "finished": true
  }];

  const competitionService = {getByChampionshipId: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildCompetitionController({competitionService});

  // Act
  await controller.getByChampionship(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(200);
  expect(response.json).toHaveBeenCalledWith(fakeServiceResult);
})

it('getCoachesCompetitions returns response with status 400 and Čempionato id yra privalomas laukas message', async () => {
  // Assert
  const request = mockRequest({}, {coachId: 1});
  const response = mockResponse();
  const fakeServiceResult = [{
    "id": 18,
    "name": "TEAM",
    "refereeId": 61,
    "resultTypeId": 1,
    "active": false,
    "finished": true
  }];

  const competitionService = {getCoachesCompetitions: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildCompetitionController({competitionService});

  // Act
  await controller.getCoachesCompetitions(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Čempionato id yra privalomas laukas']});
})

it('getCoachesCompetitions returns response with status 400 and Trenerio id yra privalomas laukas message', async () => {
  // Assert
  const request = mockRequest({}, {championshipId: 1});
  const response = mockResponse();
  const fakeServiceResult = [{
    "id": 18,
    "name": "TEAM",
    "refereeId": 61,
    "resultTypeId": 1,
    "active": false,
    "finished": true
  }];

  const competitionService = {getCoachesCompetitions: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildCompetitionController({competitionService});

  // Act
  await controller.getCoachesCompetitions(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Trenerio id yra privalomas laukas']});
})

it('getCoachesCompetitions returns response with status 400 and Trenerio id yra privalomas laukas and Čempionato id yra privalomas laukas messages', async () => {
  // Assert
  const request = mockRequest({}, {});
  const response = mockResponse();
  const fakeServiceResult = [{
    "id": 18,
    "name": "TEAM",
    "refereeId": 61,
    "resultTypeId": 1,
    "active": false,
    "finished": true
  }];

  const competitionService = {getCoachesCompetitions: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildCompetitionController({competitionService});

  // Act
  await controller.getCoachesCompetitions(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Čempionato id yra privalomas laukas', 'Trenerio id yra privalomas laukas']});
})

it('getCoachesCompetitions returns response with status 200 and service result as body', async () => {
  // Assert
  const request = mockRequest({}, {championshipId: 1, coachId: 1});
  const response = mockResponse();
  const fakeServiceResult = [{
    "id": 18,
    "name": "TEAM",
    "refereeId": 61,
    "resultTypeId": 1,
    "active": false,
    "finished": true
  }];

  const competitionService = {getCoachesCompetitions: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildCompetitionController({competitionService});

  // Act
  await controller.getCoachesCompetitions(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(200);
  expect(response.json).toHaveBeenCalledWith(fakeServiceResult);
})

it('getRefereesCompetitions returns response with status 400 and Čempionato id yra privalomas laukas message', async () => {
  // Assert
  const request = mockRequest({}, {refereeId: 1});
  const response = mockResponse();
  const fakeServiceResult = [{
    "id": 18,
    "name": "TEAM",
    "refereeId": 61,
    "resultTypeId": 1,
    "active": false,
    "finished": true
  }];

  const competitionService = {getRefereesCompetitions: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildCompetitionController({competitionService});

  // Act
  await controller.getRefereesCompetitions(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Čempionato id yra privalomas laukas']});
})

it('getRefereesCompetitions returns response with status 400 and Teisėjo id yra privalomas laukas message', async () => {
  // Assert
  const request = mockRequest({}, {championshipId: 1});
  const response = mockResponse();
  const fakeServiceResult = [{
    "id": 18,
    "name": "TEAM",
    "refereeId": 61,
    "resultTypeId": 1,
    "active": false,
    "finished": true
  }];

  const competitionService = {getRefereesCompetitions: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildCompetitionController({competitionService});

  // Act
  await controller.getRefereesCompetitions(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Teisėjo id yra privalomas laukas']});
})

it('getRefereesCompetitions returns response with status 400 and Teisėjo id yra privalomas laukas and Čempionato id yra privalomas laukas messages', async () => {
  // Assert
  const request = mockRequest({}, {});
  const response = mockResponse();
  const fakeServiceResult = [{
    "id": 18,
    "name": "TEAM",
    "refereeId": 61,
    "resultTypeId": 1,
    "active": false,
    "finished": true
  }];

  const competitionService = {getRefereesCompetitions: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildCompetitionController({competitionService});

  // Act
  await controller.getRefereesCompetitions(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Čempionato id yra privalomas laukas', 'Teisėjo id yra privalomas laukas']});
})

it('getRefereesCompetitions returns response with status 200 and service result as body', async () => {
  // Assert
  const request = mockRequest({}, {championshipId: 1, refereeId: 1});
  const response = mockResponse();
  const fakeServiceResult = [{
    "id": 18,
    "name": "TEAM",
    "refereeId": 61,
    "resultTypeId": 1,
    "active": false,
    "finished": true
  }];

  const competitionService = {getRefereesCompetitions: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildCompetitionController({competitionService});

  // Act
  await controller.getRefereesCompetitions(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(200);
  expect(response.json).toHaveBeenCalledWith(fakeServiceResult);
})

it('getSoloCompetitors returns response with status 400 and Varžybų id yra privalomas laukas message', async () => {
  // Assert
  const request = mockRequest({}, {});
  const response = mockResponse();
  const fakeServiceResult = [{
    "id": 18,
    "name": "TEAM",
    "refereeId": 61,
    "resultTypeId": 1,
    "active": false,
    "finished": true
  }];

  const competitionService = {getSoloCompetitors: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildCompetitionController({competitionService});

  // Act
  await controller.getSoloCompetitors(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Varžybų id yra privalomas laukas']});
})

it('getSoloCompetitors returns response with status 200 and service result as body', async () => {
  // Assert
  const request = mockRequest({}, {competitionId: 1});
  const response = mockResponse();
  const fakeServiceResult = [{
    "id": 18,
    "name": "TEAM",
    "refereeId": 61,
    "resultTypeId": 1,
    "active": false,
    "finished": true
  }];

  const competitionService = {getSoloCompetitors: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildCompetitionController({competitionService});

  // Act
  await controller.getSoloCompetitors(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(200);
  expect(response.json).toHaveBeenCalledWith(fakeServiceResult);
})

it('getTeamCompetitors returns response with status 400 and Varžybų id yra privalomas laukas message', async () => {
  // Assert
  const request = mockRequest({}, {});
  const response = mockResponse();
  const fakeServiceResult = [{
    "id": 18,
    "name": "TEAM",
    "refereeId": 61,
    "resultTypeId": 1,
    "active": false,
    "finished": true
  }];

  const competitionService = {getTeamCompetitors: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildCompetitionController({competitionService});

  // Act
  await controller.getTeamCompetitors(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Varžybų id yra privalomas laukas']});
})

it('getTeamCompetitors returns response with status 200 and service result as body', async () => {
  // Assert
  const request = mockRequest({}, {competitionId: 1});
  const response = mockResponse();
  const fakeServiceResult = [{
    "id": 18,
    "name": "TEAM",
    "refereeId": 61,
    "resultTypeId": 1,
    "active": false,
    "finished": true
  }];

  const competitionService = {getTeamCompetitors: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildCompetitionController({competitionService});

  // Act
  await controller.getTeamCompetitors(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(200);
  expect(response.json).toHaveBeenCalledWith(fakeServiceResult);
})

it('create returns response with status 400 and pavadinimas yra privalomas message', async () => {
  // Assert
  const request = mockRequest({refereeId: 1, championshipId: 1, resultTypeId: 1});
  const response = mockResponse();
  const fakeCompetition = {
    "id": 2,
    "name": "competition",
    "refereeId": 48,
    "championshipId": 1,
    "resultTypeId": 1
  };

  const competitionService = {createCompetition: jest.fn().mockResolvedValue(fakeCompetition)}
  const controller = buildCompetitionController({competitionService});

  // Act
  await controller.create(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Pavadinimas yra privalomas laukas']});
})

it('create returns response with status 400 and Teisėjo id yra privalomas laukas', async () => {
  // Assert
  const request = mockRequest({name: 'volleyball', championshipId: 1, resultTypeId: 1});
  const response = mockResponse();
  const fakeCompetition = {
    "id": 2,
    "name": "competition",
    "refereeId": 48,
    "championshipId": 1,
    "resultTypeId": 1
  };

  const competitionService = {createCompetition: jest.fn().mockResolvedValue(fakeCompetition)}
  const controller = buildCompetitionController({competitionService});

  // Act
  await controller.create(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Teisėjo id yra privalomas laukas']});
})

it('create returns response with status 400 and Čempionato id yra privalomas laukas', async () => {
  // Assert
  const request = mockRequest({name: 'volleyball', refereeId: 1, resultTypeId: 1});
  const response = mockResponse();
  const fakeCompetition = {
    "id": 2,
    "name": "competition",
    "refereeId": 48,
    "championshipId": 1,
    "resultTypeId": 1
  };

  const competitionService = {createCompetition: jest.fn().mockResolvedValue(fakeCompetition)}
  const controller = buildCompetitionController({competitionService});

  // Act
  await controller.create(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Čempionato id yra privalomas laukas']});
})

it('create returns response with status 400 and Rezultato tipo id yra privalomas laukas', async () => {
  // Assert
  const request = mockRequest({name: 'volleyball', refereeId: 1, championshipId: 1});
  const response = mockResponse();
  const fakeCompetition = {
    "id": 2,
    "name": "competition",
    "refereeId": 48,
    "championshipId": 1,
    "resultTypeId": 1
  };

  const competitionService = {createCompetition: jest.fn().mockResolvedValue(fakeCompetition)}
  const controller = buildCompetitionController({competitionService});

  // Act
  await controller.create(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Rezultato tipo id yra privalomas laukas']});
})

it('create returns response with status 201 and serviceReturned result body', async () => {
  // Assert
  const request = mockRequest({name: 'volleyball', refereeId: 1, championshipId: 1, resultTypeId: 1});
  const response = mockResponse();
  const fakeCompetition = {
    "id": 2,
    "name": "competition",
    "refereeId": 48,
    "championshipId": 1,
    "resultTypeId": 1
  };

  const competitionService = {createCompetition: jest.fn().mockResolvedValue(fakeCompetition)}
  const controller = buildCompetitionController({competitionService});

  // Act
  await controller.create(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(201);
  expect(response.json).toHaveBeenCalledWith(fakeCompetition);
})

it('registerTeam returns response with status 400 and Varžybų id yra privalomas laukas message', async () => {
  // Assert
  const request = mockRequest({teamId: 1});
  const response = mockResponse();
  const competitionService = {registerTeam: jest.fn().mockResolvedValue('')}
  const controller = buildCompetitionController({competitionService});

  // Act
  await controller.registerTeam(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Varžybų id yra privalomas laukas']});
})

it('registerTeam returns response with status 400 and Komandos id yra privalomas laukas message', async () => {
  // Assert
  const request = mockRequest({competitionId: 1});
  const response = mockResponse();
  const competitionService = {registerTeam: jest.fn().mockResolvedValue('')}
  const controller = buildCompetitionController({competitionService});

  // Act
  await controller.registerTeam(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Komandos id yra privalomas laukas']});
})

it('registerTeam returns response with status 400 and Komandos id yra privalomas laukas message', async () => {
  // Assert
  const request = mockRequest({competitionId: 1, teamId: 1});
  const response = mockResponse();
  const fakeError = 'fake error';
  const competitionService = {registerTeam: jest.fn().mockResolvedValue({error: fakeError})}
  const controller = buildCompetitionController({competitionService});

  // Act
  await controller.registerTeam(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: [fakeError]});
})

it('registerTeam returns response with status 400 and Komandos id yra privalomas laukas message', async () => {
  // Assert
  const request = mockRequest({competitionId: 1, teamId: 1});
  const response = mockResponse();
  const competitionService = {registerTeam: jest.fn().mockResolvedValue({error: ''})}
  const controller = buildCompetitionController({competitionService});

  // Act
  await controller.registerTeam(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(201);
  expect(response.json).toHaveBeenCalledWith('Komanda sėkmingai registruota į varžybas');
})

it('registerTeamMember returns response with status 400 and Varžybų id yra privalomas laukas message', async () => {
  // Assert
  const request = mockRequest({teamId: 1, teamMemberId: 1});
  const response = mockResponse();
  const competitionService = {registerTeamMember: jest.fn().mockResolvedValue('')}
  const controller = buildCompetitionController({competitionService});

  // Act
  await controller.registerTeamMember(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Varžybų id yra privalomas laukas']});
})

it('registerTeamMember returns response with status 400 and Komandos id yra privalomas laukas message', async () => {
  // Assert
  const request = mockRequest({competitionId: 1, teamMemberId: 1});
  const response = mockResponse();
  const competitionService = {registerTeamMember: jest.fn().mockResolvedValue('')}
  const controller = buildCompetitionController({competitionService});

  // Act
  await controller.registerTeamMember(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Komandos id yra privalomas laukas']});
})

it('registerTeamMember returns response with status 400 and Komandos nario id yra privalomas laukas message', async () => {
  // Assert
  const request = mockRequest({competitionId: 1, teamId: 1});
  const response = mockResponse();
  const competitionService = {registerTeamMember: jest.fn().mockResolvedValue('')}
  const controller = buildCompetitionController({competitionService});

  // Act
  await controller.registerTeamMember(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Komandos nario id yra privalomas laukas']});
})

it('registerTeamMember returns response with status 400 and fake error message', async () => {
  // Assert
  const request = mockRequest({competitionId: 1, teamId: 1, teamMemberId: 1});
  const response = mockResponse();
  const fakeError = 'fake error';
  const competitionService = {registerTeamMember: jest.fn().mockResolvedValue({error: fakeError})}
  const controller = buildCompetitionController({competitionService});

  // Act
  await controller.registerTeamMember(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: [fakeError]});
})

it('registerTeamMember returns response with status 200 and Komanda ir komandos narys sėkmingai registruoti į varžybas message', async () => {
  // Assert
  const request = mockRequest({competitionId: 1, teamId: 1, teamMemberId: 1});
  const response = mockResponse();
  const competitionService = {registerTeamMember: jest.fn().mockResolvedValue({error: ''})}
  const controller = buildCompetitionController({competitionService});

  // Act
  await controller.registerTeamMember(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(201);
  expect(response.json).toHaveBeenCalledWith('Komanda ir komandos narys sėkmingai registruoti į varžybas');
})

it('activate returns response with status 400 and Id yra privalomas laukas message', async () => {
  // Assert
  const request = mockRequest({}, {});
  const response = mockResponse();
  const competitionService = {activateCompetition: jest.fn().mockResolvedValue(true)}
  const controller = buildCompetitionController({competitionService});

  // Act
  await controller.activate(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Id yra privalomas laukas']});
})

it('activate returns response with status 400 and Nepavyko aktyvuoti varžybų su id message', async () => {
  // Assert
  const competitionId = 1;
  const request = mockRequest({}, {id: competitionId});
  const response = mockResponse();
  const competitionService = {activateCompetition: jest.fn().mockResolvedValue(false)}
  const controller = buildCompetitionController({competitionService});

  // Act
  await controller.activate(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: [`Nepavyko aktyvuoti varžybų su id = ${competitionId}`]});
})

it('activate returns response with status 200 and Varžybos sėkmingai aktyvuotos message', async () => {
  // Assert
  const request = mockRequest({}, {id: 1});
  const response = mockResponse();
  const competitionService = {activateCompetition: jest.fn().mockResolvedValue(true)}
  const controller = buildCompetitionController({competitionService});

  // Act
  await controller.activate(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(200);
  expect(response.json).toHaveBeenCalledWith('Varžybos sėkmingai aktyvuotos');
})

it('edit returns response with status 400 and Id yra privalomas laukas message', async () => {
  // Assert
  const request = mockRequest({name: 'competition', refereeId: 1, resultTypeId: 1}, {});
  const response = mockResponse();
  const competitionService = {editCompetition: jest.fn().mockResolvedValue(true)}
  const controller = buildCompetitionController({competitionService});

  // Act
  await controller.edit(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Id yra privalomas laukas']});
})

it('edit returns response with status 400 and Pavadinimas yra privalomas laukas message', async () => {
  // Assert
  const request = mockRequest({refereeId: 1, resultTypeId: 1}, {id: 1});
  const response = mockResponse();
  const competitionService = {editCompetition: jest.fn().mockResolvedValue(true)}
  const controller = buildCompetitionController({competitionService});

  // Act
  await controller.edit(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Pavadinimas yra privalomas laukas']});
})

it('edit returns response with status 400 and Teisėjo id yra privalomas laukas message', async () => {
  // Assert
  const request = mockRequest({name: 'competition', resultTypeId: 1}, {id: 1});
  const response = mockResponse();
  const competitionService = {editCompetition: jest.fn().mockResolvedValue(true)}
  const controller = buildCompetitionController({competitionService});

  // Act
  await controller.edit(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Teisėjo id yra privalomas laukas']});
})

it('edit returns response with status 400 and Rezultato tipo id yra privalomas laukas message', async () => {
  // Assert
  const request = mockRequest({name: 'competition', refereeId: 1}, {id: 1});
  const response = mockResponse();
  const competitionService = {editCompetition: jest.fn().mockResolvedValue(true)}
  const controller = buildCompetitionController({competitionService});

  // Act
  await controller.edit(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Rezultato tipo id yra privalomas laukas']});
})

it('edit returns response with status 200 and Varžybos sėkmingai redaguotos message', async () => {
  // Assert
  const request = mockRequest({name: 'competition', refereeId: 1, resultTypeId: 1}, {id: 1});
  const response = mockResponse();
  const competitionService = {editCompetition: jest.fn().mockResolvedValue(true)}
  const controller = buildCompetitionController({competitionService});

  // Act
  await controller.edit(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(200);
  expect(response.json).toHaveBeenCalledWith('Varžybos sėkmingai redaguotos');
})

it('remove returns response with status 400 and Id yra privalomas laukas message', async () => {
  // Assert
  const request = mockRequest({}, {});
  const response = mockResponse();
  const competitionService = {deleteCompetition: jest.fn().mockResolvedValue('')}
  const controller = buildCompetitionController({competitionService});

  // Act
  await controller.remove(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Id yra privalomas laukas']});
})

it('remove returns response with status 400 and service error message', async () => {
  // Assert
  const request = mockRequest({}, {id: 1});
  const response = mockResponse();
  const fakeError = 'fake error';
  const competitionService = {deleteCompetition: jest.fn().mockResolvedValue(fakeError)}
  const controller = buildCompetitionController({competitionService});

  // Act
  await controller.remove(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: [fakeError]});
})

it('remove returns response with status 200 and Varžybos sėkmingai ištrintos message', async () => {
  // Assert
  const request = mockRequest({}, {id: 1});
  const response = mockResponse();
  const competitionService = {deleteCompetition: jest.fn().mockResolvedValue('')}
  const controller = buildCompetitionController({competitionService});

  // Act
  await controller.remove(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(200);
  expect(response.json).toHaveBeenCalledWith('Varžybos sėkmingai ištrintos');
})