const mockRequest = require('../mockRequest');
const mockResponse = require('../mockResponse');
const buildAuthController = require('../../src/controllers/auth.controller');

it('Login returns response with status 400 and username is required validation message', async () => {
  // Assert
  const fakeToken = 'fakeToken';
  const request = mockRequest({password: 'password'});
  const response = mockResponse();
  const authenticationService = {authenticateUser: jest.fn().mockResolvedValue(fakeToken)};
  const controller = buildAuthController({authenticationService});

  // Act
  await controller.login(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Prisijungimo vardas yra privalomas laukas']});
})

it('Login returns response with status 400 and username is required validation message', async () => {
  // Assert
  const fakeToken = 'fakeToken';
  const request = mockRequest({username: 'username'});
  const response = mockResponse();
  const authenticationService = {authenticateUser: jest.fn().mockResolvedValue(fakeToken)};
  const controller = buildAuthController({authenticationService});

  // Act
  await controller.login(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Slaptažodis yra privalomas laukas']});
})

it('Login returns response with status 200 and passed x-auth-token fakeToken', async () => {
  // Assert
  const fakeToken = 'fakeToken';
  const request = mockRequest({username: 'username', password: 'password'});
  const response = mockResponse();
  const authenticationService = {authenticateUser: jest.fn().mockResolvedValue(fakeToken)};
  const controller = buildAuthController({authenticationService});

  // Act
  await controller.login(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(200);
  expect(response.header).toHaveBeenCalledWith('x-auth-token', fakeToken);
  expect(response.json).toHaveBeenCalledWith();
});

it('Login returns response with status 401 and empty response body', async () => {
  // Assert
  const request = mockRequest({username: 'username', password: 'password'});
  const response = mockResponse();
  const authenticationService = {authenticateUser: jest.fn().mockResolvedValue('')};
  const controller = buildAuthController({authenticationService});

  // Act
  await controller.login(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(401);
  expect(response.json).toHaveBeenCalledWith();
});
