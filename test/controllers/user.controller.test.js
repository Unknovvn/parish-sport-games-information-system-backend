const mockRequest = require('../mockRequest');
const mockResponse = require('../mockResponse');
const buildUserController = require('../../src/controllers/user.controller');

it('getAll returns response with status 200 and service result as body', async () => {
  // Assert
  const request = mockRequest({}, {});
  const response = mockResponse();
  const fakeServiceResult = [
    {
      "id": 65,
      "name": "Referee",
      "surname": "TEST",
      "roleId": 2
    },
    {
      "id": 66,
      "name": "Coach1",
      "surname": "TEST",
      "roleId": 3
    }
  ];

  const userService = {getAllUsers: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildUserController({userService});

  // Act
  await controller.getAll(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(200);
  expect(response.json).toHaveBeenCalledWith(fakeServiceResult);
})

it('getById returns response with status 400 and Id yra privalomas laukas message', async () => {
  // Assert
  const request = mockRequest({}, {});
  const response = mockResponse();
  const fakeServiceResult =
    {
      "id": 65,
      "name": "Referee",
      "surname": "TEST",
      "roleId": 2
    };

  const userService = {getUserById: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildUserController({userService});

  // Act
  await controller.getById(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Id yra privalomas laukas']});
})

it('getById returns response with status 400 and Nėra vartotojo su id message', async () => {
  // Assert
  const userId = 1;
  const request = mockRequest({}, {id: userId});
  const response = mockResponse();
  const fakeServiceResult = null;
  const userService = {getUserById: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildUserController({userService});

  // Act
  await controller.getById(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: [`Nėra vartotojo su id = ${userId}`]});
})

it('getById returns response with status 200 and service result as body', async () => {
  // Assert
  const request = mockRequest({}, {id: 1});
  const response = mockResponse();
  const fakeServiceResult =
    {
      "id": 65,
      "name": "Referee",
      "surname": "TEST",
      "roleId": 2
    };

  const userService = {getUserById: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildUserController({userService});

  // Act
  await controller.getById(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(200);
  expect(response.json).toHaveBeenCalledWith(fakeServiceResult);
})

it('getByRole returns response with status 400 and Rolės id yra privalomas laukas message', async () => {
  // Assert
  const request = mockRequest({}, {});
  const response = mockResponse();
  const fakeServiceResult = [
    {
      "id": 65,
      "name": "Referee",
      "surname": "TEST",
      "roleId": 2
    },
    {
      "id": 66,
      "name": "Coach1",
      "surname": "TEST",
      "roleId": 3
    }
  ];

  const userService = {getUsersByRole: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildUserController({userService});

  // Act
  await controller.getByRole(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Rolės id yra privalomas laukas']});
})

it('getByRole returns response with status 200 and service result as body', async () => {
  // Assert
  const request = mockRequest({}, {roleId: 1});
  const response = mockResponse();
  const fakeServiceResult = [
    {
      "id": 65,
      "name": "Referee",
      "surname": "TEST",
      "roleId": 2
    },
    {
      "id": 66,
      "name": "Coach1",
      "surname": "TEST",
      "roleId": 3
    }
  ];

  const userService = {getUsersByRole: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildUserController({userService});

  // Act
  await controller.getByRole(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(200);
  expect(response.json).toHaveBeenCalledWith(fakeServiceResult);
})

it('create returns response with status 400 and Vardas yra privalomas laukas message', async () => {
  // Assert
  const request = mockRequest({surname: 'surname', login: 'login', password: 'password', roleId: 1}, {});
  const response = mockResponse();
  const fakeServiceResult = {
    "id": 65,
    "name": "Referee",
    "surname": "TEST",
    "roleId": 2
  }

  const userService = {createUser: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildUserController({userService});

  // Act
  await controller.create(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Vardas yra privalomas laukas']});
})

it('create returns response with status 400 and Pavardė yra privalomas laukas message', async () => {
  // Assert
  const request = mockRequest({name: 'name', login: 'login', password: 'password', roleId: 1}, {});
  const response = mockResponse();
  const fakeServiceResult = {
    "id": 65,
    "name": "Referee",
    "surname": "TEST",
    "roleId": 2
  }

  const userService = {createUser: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildUserController({userService});

  // Act
  await controller.create(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Pavardė yra privalomas laukas']});
})

it('create returns response with status 400 and Vartotojo vardas yra privalomas laukas message', async () => {
  // Assert
  const request = mockRequest({name: 'name', surname: 'surname', password: 'password', roleId: 1}, {});
  const response = mockResponse();
  const fakeServiceResult = {
    "id": 65,
    "name": "Referee",
    "surname": "TEST",
    "roleId": 2
  }

  const userService = {createUser: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildUserController({userService});

  // Act
  await controller.create(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Vartotojo vardas yra privalomas laukas']});
})

it('create returns response with status 400 and Slaptažodis yra privalomas laukas message', async () => {
  // Assert
  const request = mockRequest({name: 'name', surname: 'surname', login: 'login', roleId: 1}, {});
  const response = mockResponse();
  const fakeServiceResult = {
    "id": 65,
    "name": "Referee",
    "surname": "TEST",
    "roleId": 2
  }

  const userService = {createUser: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildUserController({userService});

  // Act
  await controller.create(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Slaptažodis yra privalomas laukas']});
})

it('create returns response with status 400 and Rolės id yra privalomas laukas message', async () => {
  // Assert
  const request = mockRequest({name: 'name', surname: 'surname', login: 'login', password: 'password'}, {});
  const response = mockResponse();
  const fakeServiceResult = {
    "id": 65,
    "name": "Referee",
    "surname": "TEST",
    "roleId": 2
  }

  const userService = {createUser: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildUserController({userService});

  // Act
  await controller.create(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Rolės id yra privalomas laukas']});
})

it('create returns response with status 400 and service error message', async () => {
  // Assert
  const request = mockRequest({name: 'name', surname: 'surname', login: 'login', password: 'password', roleId: 1}, {});
  const response = mockResponse();
  const fakeError = 'fake error';
  const fakeServiceResult = {
    id: 65,
    name: "Referee",
    surname: "TEST",
    roleId: 2,
    error: fakeError
  }

  const userService = {createUser: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildUserController({userService});

  // Act
  await controller.create(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: [fakeError]});
})

it('create returns response with status 201 and service result as body', async () => {
  // Assert
  const request = mockRequest({name: 'name', surname: 'surname', login: 'login', password: 'password', roleId: 1}, {});
  const response = mockResponse();
  const fakeServiceResult = {
    "id": 65,
    "name": "Referee",
    "surname": "TEST",
    "roleId": 2
  }

  const userService = {createUser: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildUserController({userService});

  // Act
  await controller.create(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(201);
  expect(response.json).toHaveBeenCalledWith(fakeServiceResult);
})

it('editPassword returns response with status 400 and Id yra privalomas laukas message', async () => {
  // Assert
  const request = mockRequest({password: 'password', newPassword: 'newPassword'}, {});
  const response = mockResponse();
  const userService = {updatePassword: jest.fn().mockResolvedValue(true)}
  const controller = buildUserController({userService});

  // Act
  await controller.editPassword(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Id yra privalomas laukas']});
})

it('editPassword returns response with status 400 and Slaptažodis yra privalomas laukas message', async () => {
  // Assert
  const request = mockRequest({newPassword: 'newPassword'}, {id: 1});
  const response = mockResponse();
  const userService = {updatePassword: jest.fn().mockResolvedValue(true)}
  const controller = buildUserController({userService});

  // Act
  await controller.editPassword(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Slaptažodis yra privalomas laukas']});
})

it('editPassword returns response with status 400 and Naujas slaptažodis yra privalomas laukas message', async () => {
  // Assert
  const request = mockRequest({password: 'password'}, {id: 1});
  const response = mockResponse();
  const userService = {updatePassword: jest.fn().mockResolvedValue(true)}
  const controller = buildUserController({userService});

  // Act
  await controller.editPassword(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Naujas slaptažodis yra privalomas laukas']});
})

it('editPassword returns response with status 400 and Nepavyko redaguoti vartotojo su id message', async () => {
  // Assert
  const userId = 1;
  const request = mockRequest({password: 'password', newPassword: 'newPassword'}, {id: userId});
  const response = mockResponse();
  const userService = {updatePassword: jest.fn().mockResolvedValue(false)}
  const controller = buildUserController({userService});

  // Act
  await controller.editPassword(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: [`Nepavyko redaguoti vartotojo su id = ${userId} duomenys`]});
})

it('editPassword returns response with status 200 and Vartotojo duomenys sėkmingai redaguotas message', async () => {
  // Assert
  const request = mockRequest({password: 'password', newPassword: 'newPassword'}, {id: 1});
  const response = mockResponse();
  const userService = {updatePassword: jest.fn().mockResolvedValue(true)}
  const controller = buildUserController({userService});

  // Act
  await controller.editPassword(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(200);
  expect(response.json).toHaveBeenCalledWith('Vartotojo duomenys sėkmingai redaguotas');
})

it('remove returns response with status 400 and Id yra privalomas laukas message', async () => {
  // Assert
  const request = mockRequest({}, {});
  const response = mockResponse();
  const userService = {deleteUser: jest.fn().mockResolvedValue('')}
  const controller = buildUserController({userService});

  // Act
  await controller.remove(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Id yra privalomas laukas']});
})

it('remove returns response with status 400 and server error message', async () => {
  // Assert
  const request = mockRequest({}, {id: 1});
  const response = mockResponse();
  const fakeError = 'fake error';
  const userService = {deleteUser: jest.fn().mockResolvedValue(fakeError)}
  const controller = buildUserController({userService});

  // Act
  await controller.remove(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: [fakeError]});
})

it('remove returns response with status 200 and Vartotojas sėkmingai ištrintas message', async () => {
  // Assert
  const request = mockRequest({}, {id: 1});
  const response = mockResponse();
  const userService = {deleteUser: jest.fn().mockResolvedValue('')}
  const controller = buildUserController({userService});

  // Act
  await controller.remove(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(200);
  expect(response.json).toHaveBeenCalledWith('Vartotojas sėkmingai ištrintas');
})