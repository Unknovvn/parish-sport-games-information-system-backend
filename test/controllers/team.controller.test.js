const mockRequest = require('../mockRequest');
const mockResponse = require('../mockResponse');
const buildTeamController = require('../../src/controllers/team.controller');

it('getCoachesTeam returns response with status 400 and Čempionato id yra privalomas laukas message', async () => {
  // Assert
  const request = mockRequest({}, {coachId: 1});
  const response = mockResponse();
  const fakeServiceResult = [{
    "id": 1,
    "name": "edited_name",
    "members": [
      {
        "id": 2,
        "name": "Andžej",
        "surname": "Random"
      }
    ]
  }];

  const teamService = {getByChampionshipIdAndCoachId: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildTeamController({teamService});

  // Act
  await controller.getCoachesTeam(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Čempionato id yra privalomas laukas']});
})

it('getCoachesTeam returns response with status 400 and Trenerio id yra privalomas laukas message', async () => {
  // Assert
  const request = mockRequest({}, {championshipId: 1});
  const response = mockResponse();
  const fakeServiceResult = [{
    "id": 1,
    "name": "edited_name",
    "members": [
      {
        "id": 2,
        "name": "Andžej",
        "surname": "Random"
      }
    ]
  }];

  const teamService = {getByChampionshipIdAndCoachId: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildTeamController({teamService});

  // Act
  await controller.getCoachesTeam(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Trenerio id yra privalomas laukas']});
})

it('getCoachesTeam returns response with status 200 and service result as body', async () => {
  // Assert
  const request = mockRequest({}, {championshipId: 1, coachId: 1});
  const response = mockResponse();
  const fakeServiceResult = [{
    "id": 1,
    "name": "edited_name",
    "members": [
      {
        "id": 2,
        "name": "Andžej",
        "surname": "Random"
      }
    ]
  }];

  const teamService = {getByChampionshipIdAndCoachId: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildTeamController({teamService});

  // Act
  await controller.getCoachesTeam(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(200);
  expect(response.json).toHaveBeenCalledWith(fakeServiceResult);
})

it('create returns response with status 400 and Pavadinimas yra privalomas laukas message', async() => {
  // Assert
  const request = mockRequest({members: [{name: 'member name'}], coachId: 1, championshipId: 1}, {});
  const response = mockResponse();
  const fakeServiceResult = [{
    "id": 1,
    "name": "edited_name",
    "members": [
      {
        "id": 2,
        "name": "Andžej",
        "surname": "Random"
      }
    ]
  }];

  const teamService = {create: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildTeamController({teamService});

  // Act
  await controller.create(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Pavadinimas yra privalomas laukas']});
})

it('create returns response with status 400 and Komandos nariai yra privalomas laukas message', async() => {
  // Assert
  const request = mockRequest({name: 'name', coachId: 1, championshipId: 1}, {});
  const response = mockResponse();
  const fakeServiceResult = [{
    "id": 1,
    "name": "edited_name",
    "members": [
      {
        "id": 2,
        "name": "Andžej",
        "surname": "Random"
      }
    ]
  }];

  const teamService = {create: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildTeamController({teamService});

  // Act
  await controller.create(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Komandos nariai yra privalomas laukas']});
})

it('create returns response with status 400 and Trenerio id yra privalomas laukas message', async() => {
  // Assert
  const request = mockRequest({name: 'name', members: [{name: 'member name'}], championshipId: 1}, {});
  const response = mockResponse();
  const fakeServiceResult = [{
    "id": 1,
    "name": "edited_name",
    "members": [
      {
        "id": 2,
        "name": "Andžej",
        "surname": "Random"
      }
    ]
  }];

  const teamService = {create: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildTeamController({teamService});

  // Act
  await controller.create(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Trenerio id yra privalomas laukas']});
})

it('create returns response with status 400 and Čempionato id yra privalomas laukas message', async() => {
  // Assert
  const request = mockRequest({name: 'name', members: [{name: 'member name'}], coachId: 1}, {});
  const response = mockResponse();
  const fakeServiceResult = [{
    "id": 1,
    "name": "edited_name",
    "members": [
      {
        "id": 2,
        "name": "Andžej",
        "surname": "Random"
      }
    ]
  }];

  const teamService = {create: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildTeamController({teamService});

  // Act
  await controller.create(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Čempionato id yra privalomas laukas']});
})

it('create returns response with status 201 and service result as body', async() => {
  // Assert
  const request = mockRequest({name: 'name', members: [{name: 'member name'}], coachId: 1, championshipId: 1}, {});
  const response = mockResponse();
  const fakeServiceResult = [{
    "id": 1,
    "name": "edited_name",
    "members": [
      {
        "id": 2,
        "name": "Andžej",
        "surname": "Random"
      }
    ]
  }];

  const teamService = {create: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildTeamController({teamService});

  // Act
  await controller.create(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(201);
  expect(response.json).toHaveBeenCalledWith(fakeServiceResult);
})

it('edit returns response status 400 and Id yra privalomas laukas message', async () => {
  // Assert
  const request = mockRequest({name: 'name', members: [{name: 'member name'}]}, {});
  const response = mockResponse();
  const teamService = {update: jest.fn().mockResolvedValue(true)}
  const controller = buildTeamController({teamService});

  // Act
  await controller.edit(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Id yra privalomas laukas']});
})

it('edit returns response status 400 and Pavadinimas yra privalomas laukas message', async () => {
  // Assert
  const request = mockRequest({members: [{name: 'member name'}]}, {id: 1});
  const response = mockResponse();
  const teamService = {update: jest.fn().mockResolvedValue(true)}
  const controller = buildTeamController({teamService});

  // Act
  await controller.edit(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Pavadinimas yra privalomas laukas']});
})

it('edit returns response status 400 and Komandos nariai yra privalomas laukas message', async () => {
  // Assert
  const request = mockRequest({name: 'name'}, {id: 1});
  const response = mockResponse();
  const teamService = {update: jest.fn().mockResolvedValue(true)}
  const controller = buildTeamController({teamService});

  // Act
  await controller.edit(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Komandos nariai yra privalomas laukas']});
})

it('edit returns response status 400 and Nepavyko redaguoti varžybų su id message', async () => {
  // Assert
  const competitionId = 1;
  const request = mockRequest({name: 'name', members: [{name: 'member name'}]}, {id: competitionId});
  const response = mockResponse();
  const teamService = {update: jest.fn().mockResolvedValue(false)}
  const controller = buildTeamController({teamService});

  // Act
  await controller.edit(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: [`Nepavyko redaguoti varžybų su id = ${competitionId}`]});
})

it('edit returns response status 200 and Varžybos sėkmingai redaguotos message', async () => {
  // Assert
  const request = mockRequest({name: 'name', members: [{name: 'member name'}]}, {id: 1});
  const response = mockResponse();
  const teamService = {update: jest.fn().mockResolvedValue(true)}
  const controller = buildTeamController({teamService});

  // Act
  await controller.edit(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(200);
  expect(response.json).toHaveBeenCalledWith('Varžybos sėkmingai redaguotos');
})

it('remove returns response status 400 and Id yra privalomas laukas message', async() => {
  // Assert
  const request = mockRequest({}, {});
  const response = mockResponse();
  const teamService = {delete: jest.fn().mockResolvedValue('')}
  const controller = buildTeamController({teamService});

  // Act
  await controller.remove(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Id yra privalomas laukas']});
})

it('remove returns response status 400 and server error message message', async() => {
  // Assert
  const request = mockRequest({}, {id: 1});
  const response = mockResponse();
  const fakeError = 'fake error';
  const teamService = {delete: jest.fn().mockResolvedValue(fakeError)}
  const controller = buildTeamController({teamService});

  // Act
  await controller.remove(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: [fakeError]});
})

it('remove returns response status 200 and Komanda sėkmingai ištrinta message', async() => {
  // Assert
  const request = mockRequest({}, {id: 1});
  const response = mockResponse();
  const teamService = {delete: jest.fn().mockResolvedValue('')}
  const controller = buildTeamController({teamService});

  // Act
  await controller.remove(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(200);
  expect(response.json).toHaveBeenCalledWith('Komanda sėkmingai ištrinta');
})