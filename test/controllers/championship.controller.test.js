const mockRequest = require('../mockRequest');
const mockResponse = require('../mockResponse');
const buildChampionshipController = require('../../src/controllers/championship.controller');

it('getAll returns response with status 200 and service result as body', async () => {
  // Assert
  const request = mockRequest();
  const response = mockResponse();
  const fakeServiceResult = [{
    "id": 9,
    "name": "Parafiada 2020",
    "date": "2020-06-16T21:00:00.000Z",
    "active": false,
    "finished": true
  }];

  const championshipService = {get: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildChampionshipController({championshipService});

  // Act
  await controller.getAll(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(200);
  expect(response.json).toHaveBeenCalledWith(fakeServiceResult);
})

it('getCoachesChampionships returns response with status 400 and coachId is required message', async () => {
  // Assert
  const request = mockRequest({}, {});
  const response = mockResponse();
  const fakeServiceResult = [{
    "id": 9,
    "name": "Parafiada 2020",
    "date": "2020-06-16T21:00:00.000Z",
    "active": false,
    "finished": true
  }];

  const championshipService = {getCoachesChampionships: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildChampionshipController({championshipService});

  // Act
  await controller.getCoachesChampionship(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Trenerio id yra privalomas laukas']});
})

it('getCoachesChampionships returns response with status 200 and service result as body', async () => {
  // Assert
  const request = mockRequest({}, {coachId: 1});
  const response = mockResponse();
  const fakeServiceResult = [{
    "id": 9,
    "name": "Parafiada 2020",
    "date": "2020-06-16T21:00:00.000Z",
    "active": false,
    "finished": true
  }];

  const championshipService = {getCoachesChampionships: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildChampionshipController({championshipService});

  // Act
  await controller.getCoachesChampionship(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(200);
  expect(response.json).toHaveBeenCalledWith(fakeServiceResult);
})

it('getActive returns response with status 200 and service result as body', async () => {
  // Assert
  const request = mockRequest();
  const response = mockResponse();
  const fakeServiceResult = [{
    "id": 9,
    "name": "Parafiada 2020",
    "date": "2020-06-16T21:00:00.000Z",
    "active": false,
    "finished": true
  }];

  const championshipService = {getActive: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildChampionshipController({championshipService});

  // Act
  await controller.getActive(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(200);
  expect(response.json).toHaveBeenCalledWith(fakeServiceResult);
})

it('getFinished returns response with status 200 and service result as body', async () => {
  // Assert
  const request = mockRequest();
  const response = mockResponse();
  const fakeServiceResult = [{
    "id": 9,
    "name": "Parafiada 2020",
    "date": "2020-06-16T21:00:00.000Z",
    "active": false,
    "finished": true
  }];

  const championshipService = {getFinished: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildChampionshipController({championshipService});

  // Act
  await controller.getFinished(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(200);
  expect(response.json).toHaveBeenCalledWith(fakeServiceResult);
})

it('getResults returns response with status 400 and id is required message', async () => {
  // Assert
  const request = mockRequest({}, {});
  const response = mockResponse();
  const fakeServiceResult = [{
    "teamName": "Komanda1",
    "points": 12
  }];

  const championshipService = {getResults: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildChampionshipController({championshipService});

  // Act
  await controller.getResults(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Id yra privalomas laukas']});
})

it('getResults returns response with status 200 and service result as body', async () => {
  // Assert
  const request = mockRequest({}, {id: 1});
  const response = mockResponse();
  const fakeServiceResult = [{
    "teamName": "Komanda1",
    "points": 12
  }];

  const championshipService = {getResults: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildChampionshipController({championshipService});

  // Act
  await controller.getResults(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(200);
  expect(response.json).toHaveBeenCalledWith(fakeServiceResult);
})

it('create returns response with status 400 and pavadinimas yra privalomas message', async () => {
  // Assert
  const request = mockRequest({date: new Date().getDate()});
  const response = mockResponse();
  const fakeChampionship = {
    "id": 9,
    "name": "Parafiada 2020",
    "date": "2020-06-16T21:00:00.000Z",
    "active": false,
    "finished": true
  };

  const championshipService = {create: jest.fn().mockResolvedValue(fakeChampionship)}
  const controller = buildChampionshipController({championshipService});

  // Act
  await controller.create(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Pavadinimas yra privalomas laukas']});
})

it('create returns response with status 400 and data yra privalomas message', async () => {
  // Assert
  const request = mockRequest({name: 'championshipName'});
  const response = mockResponse();
  const fakeChampionship = {
    "id": 9,
    "name": "Parafiada 2020",
    "date": "2020-06-16T21:00:00.000Z",
    "active": false,
    "finished": true
  };

  const championshipService = {create: jest.fn().mockResolvedValue(fakeChampionship)}
  const controller = buildChampionshipController({championshipService});

  // Act
  await controller.create(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Data yra privalomas laukas']});
})

it('create returns response with status 400 and serviceReturned error message body', async () => {
  // Assert
  const request = mockRequest({name: 'championshipName', date: new Date().getDate()});
  const response = mockResponse();
  const fakeErrorMessage = 'fake error message';
  const fakeChampionship = {
    error: fakeErrorMessage
  };

  const championshipService = {create: jest.fn().mockResolvedValue(fakeChampionship)}
  const controller = buildChampionshipController({championshipService});

  // Act
  await controller.create(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: [fakeErrorMessage]});
})

it('create returns response with status 201 and serviceReturned result body', async () => {
  // Assert
  const request = mockRequest({name: 'championshipName', date: new Date().getDate()});
  const response = mockResponse();
  const fakeChampionship = {
    "id": 9,
    "name": "Parafiada 2020",
    "date": "2020-06-16T21:00:00.000Z",
    "active": false,
    "finished": true
  };

  const championshipService = {create: jest.fn().mockResolvedValue(fakeChampionship)}
  const controller = buildChampionshipController({championshipService});

  // Act
  await controller.create(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(201);
  expect(response.json).toHaveBeenCalledWith(fakeChampionship);
})

it('activate returns response with status 400 and id is required message', async () => {
  // Assert
  const request = mockRequest({}, {});
  const response = mockResponse();
  const fakeServiceResult = true;
  const championshipService = {activate: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildChampionshipController({championshipService});

  // Act
  await controller.activate(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Id yra privalomas laukas']});
})

it('activate returns response with status 400 and nepavyko aktyvuoti čempionato message', async () => {
  // Assert
  const championshipId = 1;
  const request = mockRequest({}, {id: championshipId});
  const response = mockResponse();
  const fakeServiceResult = false;
  const championshipService = {activate: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildChampionshipController({championshipService});

  // Act
  await controller.activate(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: [`Nepavyko aktyvuoti čempionato su id = ${championshipId}`]});
})

it('activate returns response with status 200 and Čempionatas sėkmingai aktyvuotas message', async () => {
  // Assert
  const request = mockRequest({}, {id: 1});
  const response = mockResponse();
  const fakeServiceResult = true;
  const championshipService = {activate: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildChampionshipController({championshipService});

  // Act
  await controller.activate(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(200);
  expect(response.json).toHaveBeenCalledWith('Čempionatas sėkmingai aktyvuotas');
})

it('finish returns response with status 400 and id is required message', async () => {
  // Assert
  const request = mockRequest({}, {});
  const response = mockResponse();
  const fakeServiceResult = true;
  const championshipService = {finish: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildChampionshipController({championshipService});

  // Act
  await controller.finish(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Id yra privalomas laukas']});
})

it('finish returns response with status 400 and nepavyko užbaigti čempionato message', async () => {
  // Assert
  const championshipId = 1;
  const request = mockRequest({}, {id: championshipId});
  const response = mockResponse();
  const fakeServiceResult = false;
  const championshipService = {finish: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildChampionshipController({championshipService});

  // Act
  await controller.finish(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: [`Nepavyko užbaigti čempionato su id = ${championshipId}`]});
})

it('finish returns response with status 200 and Čempionatas sėkmingai užbaigtas message', async () => {
  // Assert
  const request = mockRequest({}, {id: 1});
  const response = mockResponse();
  const fakeServiceResult = true;
  const championshipService = {finish: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildChampionshipController({championshipService});

  // Act
  await controller.finish(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(200);
  expect(response.json).toHaveBeenCalledWith('Čempionatas sėkmingai užbaigtas');
})

it('edit returns response with status 400 and is is required message', async () => {
  // Assert
  const request = mockRequest({}, {});
  const response = mockResponse();
  const fakeServiceResult = true;
  const championshipService = {edit: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildChampionshipController({championshipService});

  // Act
  await controller.edit(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Id yra privalomas laukas']});
})

it('edit returns response with status 400 and pavadinimas yra privalomas message', async () => {
  // Assert
  const request = mockRequest({date: new Date().getDate()}, {id: 1});
  const response = mockResponse();
  const fakeServiceResult = true;
  const championshipService = {edit: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildChampionshipController({championshipService});

  // Act
  await controller.edit(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Pavadinimas yra privalomas laukas']});
})

it('edit returns response with status 400 and data yra privalomas message', async () => {
  // Assert
  const request = mockRequest({name: 'championshipName'}, {id: 1});
  const response = mockResponse();
  const fakeServiceResult = true;
  const championshipService = {edit: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildChampionshipController({championshipService});

  // Act
  await controller.edit(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Data yra privalomas laukas']});
})

it('edit returns response with status 400 and nepavyko redaguoti čempionato message', async () => {
  // Assert
  const championshipId = 1;
  const request = mockRequest({name: 'championshipName', date: new Date().getDate()}, {id: championshipId});
  const response = mockResponse();
  const fakeServiceResult = false;
  const championshipService = {edit: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildChampionshipController({championshipService});

  // Act
  await controller.edit(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: [`Nepavyko redaguoti čempionato su id = ${championshipId}`]});
})

it('edit returns response with status 200 and Čempionatas sėkmingai redaguotas message', async () => {
  // Assert
  const championshipId = 1;
  const request = mockRequest({name: 'championshipName', date: new Date().getDate()}, {id: championshipId});
  const response = mockResponse();
  const fakeServiceResult = true;
  const championshipService = {edit: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildChampionshipController({championshipService});

  // Act
  await controller.edit(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(200);
  expect(response.json).toHaveBeenCalledWith('Čempionatas sėkmingai redaguotas');
})

it('remove returns response with status 400 and id is required message', async () => {
  // Assert
  const request = mockRequest({}, {});
  const response = mockResponse();
  const fakeServiceResult = '';
  const championshipService = {remove: jest.fn().mockResolvedValue(fakeServiceResult)}
  const controller = buildChampionshipController({championshipService});

  // Act
  await controller.remove(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: ['Id yra privalomas laukas']});
})

it('remove returns response with status 400 and service returned error message', async () => {
  // Assert
  const request = mockRequest({}, {id: 1});
  const response = mockResponse();
  const fakeErrorMessage = 'fake error';
  const championshipService = {delete: jest.fn().mockResolvedValue(fakeErrorMessage)}
  const controller = buildChampionshipController({championshipService});

  // Act
  await controller.remove(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(400);
  expect(response.json).toHaveBeenCalledWith({error: [fakeErrorMessage]});
})

it('remove returns response with status 400 and Čempionatas sėkmingai ištrintas message', async () => {
  // Assert
  const request = mockRequest({}, {id: 1});
  const response = mockResponse();
  const fakeErrorMessage = '';
  const championshipService = {delete: jest.fn().mockResolvedValue(fakeErrorMessage)}
  const controller = buildChampionshipController({championshipService});

  // Act
  await controller.remove(request, response);

  // Assert
  expect(response.status).toHaveBeenCalledWith(200);
  expect(response.json).toHaveBeenCalledWith('Čempionatas sėkmingai ištrintas');
})